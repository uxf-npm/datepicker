const TEST_REGEX = "(/tests/.*|(\\.|/)(test|spec))\\.ts$";
module.exports = {
    testRegex: TEST_REGEX,
    testPathIgnorePatterns: ["<rootDir>/.cache/", "<rootDir>/node_modules/", "<rootDir>/dist/", "<rootDir>/demo/"],
    moduleFileExtensions: ["ts", "js"],
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    collectCoverageFrom: ["<rootDir>/src/!(contexts)**/*.ts"],
    coverageReporters: ["text", "cobertura", "html"],
    globalSetup: "./global-setup.js",
    coverageThreshold: {
        global: {
            //branches: 80,
            //functions: 90,
            //lines: 90,
            //statements: 90,
        },
    },
};
