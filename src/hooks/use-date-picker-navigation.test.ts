import { renderHook, act } from "@testing-library/react-hooks";
import { useState } from "react";
import { MonthType } from "../utils/types";
import { useDatePickerNavigation, UseDatePickerNavigationReturnType } from "./use-date-picker-navigation";

const ACTIVE_MONTHS: MonthType[] = [{ date: new Date("2021-09-01"), month: 8, year: 2021 }];

test("base data", () => {
    const { result } = renderHook<null, UseDatePickerNavigationReturnType>(() =>
        useDatePickerNavigation({
            activeMonths: ACTIVE_MONTHS,
            setActiveMonths: () => undefined,
            numberOfMonths: 1,
            setFocusedDate: () => undefined,
        }),
    );

    expect(result.current.canGoToNextMonth).toBe(true);
    expect(result.current.canGoToPrevMonth).toBe(true);
    expect(result.current.canGoToNextYear).toBe(true);
    expect(result.current.canGoToPrevYear).toBe(true);
    expect(result.current.canGoToMonth(new Date("2021-08-01"))).toBe(true);
    expect(result.current.canGoToYear(new Date("2020-01-01"))).toBe(true);
});

test("base data with min and max date", () => {
    const { result } = renderHook<null, UseDatePickerNavigationReturnType>(() =>
        useDatePickerNavigation({
            activeMonths: ACTIVE_MONTHS,
            setActiveMonths: () => undefined,
            numberOfMonths: 1,
            setFocusedDate: () => undefined,
            minBookingDate: new Date("2015-05-05"),
            maxBookingDate: new Date("2025-10-11"),
        }),
    );

    expect(result.current.canGoToMonth(new Date("2021-08-01"))).toBe(true);
    expect(result.current.canGoToMonth(new Date("2015-04-01"))).toBe(false);
    expect(result.current.canGoToMonth(new Date("2025-12-01"))).toBe(false);
    expect(result.current.canGoToYear(new Date("2020-01-01"))).toBe(true);
    expect(result.current.canGoToYear(new Date("2014-01-01"))).toBe(false);
    expect(result.current.canGoToYear(new Date("2026-01-01"))).toBe(false);
});

test("actions", () => {
    const { result: state } = renderHook(() =>
        useState<MonthType[]>([{ month: 8, year: 2021, date: new Date("2021-09-01") }]),
    );
    const { result, rerender } = renderHook<null, UseDatePickerNavigationReturnType>(() =>
        useDatePickerNavigation({
            activeMonths: state.current[0],
            setActiveMonths: data => state.current[1](data),
            numberOfMonths: 1,
            setFocusedDate: () => undefined,
            minBookingDate: new Date("2020-12-25"),
            maxBookingDate: new Date("2022-03-11"),
        }),
    );

    // go to year 2020
    expect(result.current.canGoToPrevYear).toBe(true);
    act(() => {
        result.current.goToPrevYear();
        rerender();
    });
    expect(result.current.canGoToPrevYear).toBe(false);
    expect(result.current.canGoToNextYear).toBe(true);

    // go to year 2022
    act(() => {
        result.current.goToNextYear(2);
        rerender();
    });
    expect(result.current.canGoToPrevYear).toBe(true);
    expect(result.current.canGoToNextYear).toBe(false);

    // go back to year 2021
    act(() => {
        result.current.goToPrevYear();
        rerender();
    });
    expect(result.current.canGoToPrevYear).toBe(true);
    expect(result.current.canGoToNextYear).toBe(true);

    // go to prev month
    act(() => {
        result.current.goToPrevMonths();
        result.current.goToPrevMonthsByOneMonth();
        rerender();
    });
    expect(result.current.canGoToPrevMonth).toBe(true);
    expect(result.current.canGoToNextMonth).toBe(true);

    // go to next month
    act(() => {
        result.current.goToNextMonths();
        result.current.goToNextMonthsByOneMonth();
        rerender();
    });
    expect(result.current.canGoToPrevMonth).toBe(true);
    expect(result.current.canGoToNextMonth).toBe(true);

    // go to month
    act(() => {
        result.current.goToDate(new Date("2020-12-30"));
        rerender();
    });
    expect(result.current.canGoToPrevMonth).toBe(false);
    expect(result.current.canGoToNextMonth).toBe(true);
    act(() => {
        result.current.goToDate(new Date("2022-03-01"));
        rerender();
    });
    expect(result.current.canGoToPrevMonth).toBe(true);
    expect(result.current.canGoToNextMonth).toBe(false);
});
