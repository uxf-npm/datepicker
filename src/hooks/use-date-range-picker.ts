import { useCallback, useEffect, useState } from "react";
import { canSelectRange } from "../utils/can-select-range";
import { getInitialMonths } from "../utils/get-initial-months";
import { isDateBlocked } from "../utils/is-date-blocked";
import { isDateHovered } from "../utils/is-date-hovered";
import { isDateInsideRange } from "../utils/is-date-inside-range";
import { isEndDate } from "../utils/is-end-date";
import { isFirstOrLastSelectedDate } from "../utils/is-first-or-last-selected-date";
import { isInUnavailableDates } from "../utils/is-in-unavailable-dates";
import { isStartDate } from "../utils/is-start-date";
import { FirstDayOfWeek, MonthType } from "../utils/types";
import { useDatePickerNavigation, UseDatePickerNavigationReturnType } from "./use-date-picker-navigation";
import { isBrowser } from "../utils/is-browser";
import { dayjsEn } from "../utils/dayjs-en";

export const START_DATE = "startDate";
export const END_DATE = "endDate";

export type FocusedInput = "startDate" | "endDate" | null;

export interface OnDatesChangeProps {
    endDate: Date | null;
    focusedInput: FocusedInput;
    startDate: Date | null;
}

export interface UseDateRangePickerProps {
    changeActiveMonthOnSelect?: boolean;
    endDate: Date | null;
    exactMinBookingDays?: boolean;
    firstDayOfWeek?: FirstDayOfWeek;
    focusedInput: FocusedInput;
    initialVisibleMonth?: Date;
    isDateBlocked?: (date: Date) => boolean;
    maxBookingDate?: Date;
    minBookingDate?: Date;
    minBookingDays?: number;
    numberOfMonths?: number;
    onDatesChange: (data: OnDatesChangeProps) => void;
    startDate: Date | null;
    unavailableDates?: Date[];
}

export interface UseDateRangePickerReturnType extends UseDatePickerNavigationReturnType {
    activeMonths: MonthType[];
    firstDayOfWeek: FirstDayOfWeek;
    focusedDate: Date | null;
    hoveredDate: Date | null;
    isDateBlocked: (date: Date) => boolean;
    isDateFocused: (date: Date) => boolean;
    isDateHovered: (date: Date) => boolean;
    isDateInsideRange: (date: Date) => boolean;
    isDateSelected: (date: Date) => boolean;
    isEndDate: (date: Date) => boolean;
    isStartDate: (date: Date) => boolean;
    numberOfMonths: number;
    onDateFocus: (date: Date) => void;
    onDateHover: (date: Date | null) => void;
    onDateSelect: (date: Date) => void;
    onResetDates: () => void;
}

export function useDateRangePicker({
    changeActiveMonthOnSelect = false,
    endDate,
    exactMinBookingDays = false,
    firstDayOfWeek = 0,
    focusedInput,
    initialVisibleMonth,
    isDateBlocked: isDateBlockedProps = () => false,
    maxBookingDate,
    minBookingDate,
    minBookingDays = 1,
    numberOfMonths = 2,
    onDatesChange,
    startDate,
    unavailableDates = [],
}: UseDateRangePickerProps): UseDateRangePickerReturnType {
    const [activeMonths, setActiveMonths] = useState(() =>
        startDate
            ? getInitialMonths(numberOfMonths, startDate)
            : getInitialMonths(numberOfMonths, initialVisibleMonth || null),
    );
    const [hoveredDate, setHoveredDate] = useState<Date | null>(null);
    const [focusedDate, setFocusedDate] = useState<Date | null>(startDate);

    const disabledDatesByUser = (date: Date) => {
        return isInUnavailableDates(unavailableDates, date) || isDateBlockedProps(date);
    };

    const onDateFocus = useCallback(
        (date: Date) => {
            setFocusedDate(date);

            if (!focusedDate || !activeMonths.map(m => m.month).includes(dayjsEn(date).month())) {
                setActiveMonths(getInitialMonths(numberOfMonths, date));
            }
        },
        [activeMonths, focusedDate, numberOfMonths],
    );

    const _isDateSelected = (date: Date) =>
        isDateInsideRange(date, startDate, endDate) || isFirstOrLastSelectedDate(date, startDate, endDate);

    const _isStartDate = (date: Date) => isStartDate(date, startDate);

    const _isEndDate = (date: Date) => isEndDate(date, endDate);

    const _isDateInsideRange = (date: Date) => isDateInsideRange(date, startDate, endDate);

    const _isDateBlocked = (date: Date) =>
        isDateBlocked({
            date,
            minBookingDate,
            maxBookingDate,
            startDate,
            endDate,
            minBookingDays,
            isDateBlockedFn: disabledDatesByUser,
        });

    const isDateFocused = (date: Date) => (focusedDate ? dayjsEn(date).isSame(focusedDate, "day") : false);

    const _isDateHovered = (date: Date) =>
        isDateHovered({
            date,
            hoveredDate,
            startDate,
            endDate,
            minBookingDays,
            exactMinBookingDays,
            isDateBlocked: disabledDatesByUser,
        });

    useEffect(() => {
        if (!isBrowser) {
            return;
        }

        function handleKeyDown(e: KeyboardEvent) {
            if (
                (e.key === "ArrowRight" || e.key === "ArrowLeft" || e.key === "ArrowDown" || e.key === "ArrowUp") &&
                !focusedDate
            ) {
                const activeMonth = activeMonths[0];
                onDateFocus(activeMonth.date);
                setActiveMonths(getInitialMonths(numberOfMonths, activeMonth.date));
            }
        }
        window.addEventListener("keydown", handleKeyDown);

        return () => {
            window.removeEventListener("keydown", handleKeyDown);
        };
    }, [activeMonths, focusedDate, numberOfMonths, onDateFocus]);

    const onResetDates = useCallback(() => {
        onDatesChange({
            startDate: null,
            endDate: null,
            focusedInput: START_DATE,
        });
    }, [onDatesChange]);

    // eslint-disable-next-line complexity
    function onDateSelect(date: Date) {
        if (
            (focusedInput === END_DATE || focusedInput === START_DATE) &&
            minBookingDays > 0 &&
            exactMinBookingDays &&
            canSelectRange({
                minBookingDays,
                exactMinBookingDays,
                minBookingDate,
                maxBookingDate,
                isDateBlocked: disabledDatesByUser,
                startDate: date,
                endDate: null,
            })
        ) {
            onDatesChange({
                startDate: date,
                endDate: dayjsEn(date)
                    .add(minBookingDays - 1, "day")
                    .toDate(),
                focusedInput: null,
            });
        } else if (
            (((focusedInput === END_DATE && startDate && dayjsEn(date).isBefore(startDate)) ||
                (focusedInput === START_DATE && endDate && dayjsEn(date).isAfter(endDate))) &&
                !exactMinBookingDays &&
                canSelectRange({
                    minBookingDays,
                    isDateBlocked: disabledDatesByUser,
                    startDate: date,
                    endDate: null,
                })) ||
            (focusedInput === START_DATE &&
                !exactMinBookingDays &&
                canSelectRange({ minBookingDays, isDateBlocked: disabledDatesByUser, endDate, startDate: date }))
        ) {
            onDatesChange({
                endDate: null,
                startDate: date,
                focusedInput: END_DATE,
            });
        } else if (
            focusedInput === START_DATE &&
            !exactMinBookingDays &&
            canSelectRange({
                minBookingDays,
                isDateBlocked: disabledDatesByUser,
                endDate: null,
                startDate: date,
            })
        ) {
            onDatesChange({
                endDate: null,
                startDate: date,
                focusedInput: END_DATE,
            });
        } else if (
            focusedInput === END_DATE &&
            startDate &&
            !dayjsEn(date).isBefore(startDate) &&
            !exactMinBookingDays &&
            canSelectRange({ minBookingDays, isDateBlocked: disabledDatesByUser, startDate, endDate: date })
        ) {
            onDatesChange({
                startDate,
                endDate: date,
                focusedInput: null,
            });
        }

        if (
            focusedInput !== END_DATE &&
            (!focusedDate || !dayjsEn(date).isSame(focusedDate, "month")) &&
            changeActiveMonthOnSelect
        ) {
            setActiveMonths(getInitialMonths(numberOfMonths, date));
        }
    }

    // eslint-disable-next-line complexity
    function onDateHover(date: Date | null) {
        if (!date) {
            setHoveredDate(null);
        } else {
            const isNotBlocked = !_isDateBlocked(date) || (startDate && dayjsEn(date).isSame(startDate, "day"));
            const isHoveredDateAfterOrEqualMinDate = minBookingDate
                ? !dayjsEn(date).isBefore(dayjsEn(minBookingDate).subtract(1, "day"))
                : true;
            const isHoveredDateBeforeOrEqualMaxDate = maxBookingDate ? !dayjsEn(date).isAfter(maxBookingDate) : true;

            // Exact minimal booking days
            const potentialEndDate = dayjsEn(date)
                .add(minBookingDays - 1, "day")
                .toDate();
            const isPotentialEndDateAfterOrEqualMinDate = minBookingDate
                ? !dayjsEn(potentialEndDate).isBefore(minBookingDate)
                : true;
            const isPotentialEndDateBeforeOrEqualMaxDate = maxBookingDate
                ? !dayjsEn(potentialEndDate).isAfter(maxBookingDate)
                : true;
            const isExactAndInRange =
                exactMinBookingDays &&
                minBookingDays > 1 &&
                isHoveredDateAfterOrEqualMinDate &&
                isHoveredDateBeforeOrEqualMaxDate &&
                isPotentialEndDateAfterOrEqualMinDate &&
                isPotentialEndDateBeforeOrEqualMaxDate;

            // Is date in range
            const isInRange =
                startDate &&
                !endDate &&
                !exactMinBookingDays &&
                isHoveredDateAfterOrEqualMinDate &&
                isHoveredDateBeforeOrEqualMaxDate;

            // Is start date hovered and in range
            const isMinBookingDaysInRange =
                minBookingDays > 1 && startDate
                    ? dayjsEn(date).isBetween(startDate, dayjsEn(startDate).add(minBookingDays - 2, "day"))
                    : true;
            const isStartDateHoveredAndInRange =
                startDate && dayjsEn(date).isSame(startDate, "day") && isMinBookingDaysInRange;

            if (isNotBlocked && (isExactAndInRange || isInRange || isStartDateHoveredAndInRange)) {
                setHoveredDate(date);
            } else if (hoveredDate !== null) {
                setHoveredDate(null);
            }
        }
    }

    return {
        ...useDatePickerNavigation({
            activeMonths,
            maxBookingDate,
            minBookingDate,
            numberOfMonths,
            setActiveMonths,
            setFocusedDate,
        }),
        activeMonths,
        firstDayOfWeek,
        focusedDate,
        hoveredDate,
        isDateBlocked: _isDateBlocked,
        isDateFocused,
        isDateHovered: _isDateHovered,
        isDateInsideRange: _isDateInsideRange,
        isDateSelected: _isDateSelected,
        isEndDate: _isEndDate,
        isStartDate: _isStartDate,
        numberOfMonths,
        onDateFocus,
        onDateHover,
        onDateSelect,
        onResetDates,
    };
}
