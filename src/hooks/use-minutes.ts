import { useMemo } from "react";

export const labelFormatFn = (hour: number) => hour.toString().padStart(2, "0");

export type MinuteType = {
    value: number;
    label: string;
};

export interface UseMinutesProps {
    labelFormat?: (minute: number) => string;
}

export interface UseMinutesReturnType {
    minutes: MinuteType[];
}

export function useMinutes({ labelFormat = labelFormatFn }: UseMinutesProps): UseMinutesReturnType {
    const minutes = useMemo(
        () =>
            Array(12)
                .fill(0)
                .map((_, i) => ({ value: i * 5, label: labelFormat(i * 5) })),
        [labelFormat],
    );

    return {
        minutes,
    };
}
