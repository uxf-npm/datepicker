import { renderHook, act } from "@testing-library/react-hooks";
import { useState } from "react";
import { useDatePicker } from "./use-date-picker";

test("base data", () => {
    const BASE_DATE = new Date("2021-09-15");
    const { result } = renderHook(() =>
        useDatePicker({
            selectedDate: BASE_DATE,
            onDateChange: () => undefined,
            minBookingDate: new Date("2020-01-01"),
        }),
    );

    expect(result.current.firstDayOfWeek).toBe(0);
    expect(result.current.focusedDate).toStrictEqual(BASE_DATE);
    expect(result.current.hoveredDate).toBe(null);
    expect(result.current.activeMonths).toStrictEqual([
        {
            date: new Date("2021-09-01"),
            month: 8,
            year: 2021,
        },
    ]);
    expect(result.current.isDateBlocked(new Date("2020-01-01"))).toBe(false);
    expect(result.current.isDateBlocked(new Date("2019-12-31"))).toBe(true);
});

test("date focus", () => {
    const BASE_DATE = new Date("2021-09-15");
    const { result: state } = renderHook(() => useState<Date | null>(BASE_DATE));
    const { result } = renderHook(() =>
        useDatePicker({ selectedDate: state.current[0], onDateChange: date => state.current[1](date) }),
    );

    const NEW_DATE = new Date("2022-12-22");
    expect(result.current.isDateFocused(BASE_DATE)).toBe(true);
    expect(result.current.isDateFocused(NEW_DATE)).toBe(false);

    act(() => {
        result.current.onDateFocus(NEW_DATE);
    });

    expect(result.current.isDateFocused(NEW_DATE)).toBe(true);
    expect(result.current.isDateFocused(BASE_DATE)).toBe(false);
});

test("date select", () => {
    const BASE_DATE = new Date("2021-09-15");
    const { result: state } = renderHook(() => useState<Date | null>(BASE_DATE));
    const { result, rerender } = renderHook(() =>
        useDatePicker({ selectedDate: state.current[0], onDateChange: date => state.current[1](date) }),
    );

    const NEW_DATE = new Date("2022-12-22");
    expect(result.current.isDateSelected(BASE_DATE)).toBe(true);
    expect(result.current.isDateSelected(NEW_DATE)).toBe(false);

    act(() => {
        result.current.onDateSelect(NEW_DATE);
        rerender();
    });

    expect(result.current.isDateSelected(NEW_DATE)).toBe(true);
    expect(result.current.isDateSelected(BASE_DATE)).toBe(false);

    // select another
    act(() => {
        result.current.onDateSelect(BASE_DATE);
        rerender();
    });

    expect(result.current.isDateSelected(BASE_DATE)).toBe(true);

    // select save (deselect)
    act(() => {
        result.current.onDateSelect(BASE_DATE);
        rerender();
    });

    expect(result.current.isDateSelected(BASE_DATE)).toBe(false);

    // reset
    act(() => {
        result.current.onDateSelect(BASE_DATE);
        result.current.onResetDates();
        rerender();
    });

    expect(result.current.isDateSelected(BASE_DATE)).toBe(false);
});
