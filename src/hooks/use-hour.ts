import { KeyboardEvent, RefObject, useCallback, useEffect } from "react";

export interface UseHourProps {
    hour: number;
    hourRef?: RefObject<HTMLElement>;
    focusedHour: number | null;
    isHourFocused: (hour: number) => boolean;
    isHourSelected: (hour: number) => boolean;
    onHourFocus: (hour: number) => void;
    onHourSelect: (hour: number) => void;
}

export interface UseHourReturnType {
    isSelected: boolean;
    onClick?: () => void;
    onKeyDown: (e: KeyboardEvent<HTMLElement>) => void;
    tabIndex: number;
}

export function useHour({
    hour,
    hourRef,
    focusedHour,
    isHourFocused,
    isHourSelected,
    onHourFocus,
    onHourSelect,
}: UseHourProps): UseHourReturnType {
    const onClick = useCallback(() => onHourSelect(hour), [hour, onHourSelect]);

    useEffect(() => {
        if (hourRef && hourRef.current && isHourFocused(hour)) {
            hourRef.current.focus();
        }
    }, [hourRef, hour, isHourFocused]);

    const onKeyDown = useCallback(
        (e: KeyboardEvent<HTMLElement>) => {
            if (["ArrowRight", "ArrowLeft", "ArrowUp", "ArrowDown"].includes(e.key)) {
                e.preventDefault();
                switch (e.key) {
                    case "ArrowRight":
                        onHourFocus(hour + 1);
                        break;
                    case "ArrowLeft":
                        onHourFocus(hour - 1);
                        break;
                    case "ArrowUp":
                        onHourFocus(hour - 4);
                        break;
                    case "ArrowDown":
                        onHourFocus(hour + 4);
                        break;
                }
            }
        },
        [hour, onHourFocus],
    );

    return {
        isSelected: isHourSelected(hour),
        onClick: onClick,
        onKeyDown,
        tabIndex: focusedHour === null || isHourFocused(hour) ? 0 : -1,
    };
}
