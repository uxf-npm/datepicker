import { useCallback, useEffect, useState } from "react";
import { getInitialMonths } from "../utils/get-initial-months";
import { isDateBlocked } from "../utils/is-date-blocked";
import { isDateHovered } from "../utils/is-date-hovered";
import { isInUnavailableDates } from "../utils/is-in-unavailable-dates";
import { isStartDate } from "../utils/is-start-date";
import { FirstDayOfWeek, MonthType } from "../utils/types";
import { useDatePickerNavigation, UseDatePickerNavigationReturnType } from "./use-date-picker-navigation";
import { isBrowser } from "../utils/is-browser";
import { dayjsEn } from "../utils/dayjs-en";

export type OnDateChangeType = Date | null;

export interface UseDatePickerProps {
    firstDayOfWeek?: FirstDayOfWeek;
    initialVisibleMonth?: Date;
    isDateBlocked?: (date: Date) => boolean;
    maxBookingDate?: Date;
    minBookingDate?: Date;
    numberOfMonths?: number;
    onDateChange: (data: OnDateChangeType) => void;
    selectedDate: Date | null;
    unavailableDates?: Date[];
}

export interface UseDatePickerReturnType extends UseDatePickerNavigationReturnType {
    activeMonths: MonthType[];
    firstDayOfWeek: FirstDayOfWeek;
    focusedDate: Date | null;
    hoveredDate: Date | null;
    isDateBlocked: (date: Date) => boolean;
    isDateFocused: (date: Date) => boolean;
    isDateHovered: (date: Date) => boolean;
    isDateSelected: (date: Date) => boolean;
    numberOfMonths: number;
    onDateFocus: (date: Date) => void;
    onDateHover: (date: Date | null) => void;
    onDateSelect: (date: Date) => void;
    onResetDates: () => void;
}

export function useDatePicker({
    firstDayOfWeek = 0,
    initialVisibleMonth,
    isDateBlocked: isDateBlockedProps = () => false,
    maxBookingDate,
    minBookingDate,
    numberOfMonths = 1,
    onDateChange,
    selectedDate,
    unavailableDates = [],
}: UseDatePickerProps): UseDatePickerReturnType {
    const [activeMonths, setActiveMonths] = useState(() =>
        selectedDate
            ? getInitialMonths(numberOfMonths, selectedDate)
            : getInitialMonths(numberOfMonths, initialVisibleMonth || null),
    );
    const [hoveredDate, setHoveredDate] = useState<Date | null>(null);
    const [focusedDate, setFocusedDate] = useState<Date | null>(selectedDate);

    const disabledDatesByUser = (date: Date) => {
        return isInUnavailableDates(unavailableDates, date) || isDateBlockedProps(date);
    };

    const onDateFocus = useCallback(
        (date: Date) => {
            setFocusedDate(date);

            if (!focusedDate || !activeMonths.map(m => m.month).includes(dayjsEn(date).month())) {
                setActiveMonths(getInitialMonths(numberOfMonths, date));
            }
        },
        [activeMonths, focusedDate, numberOfMonths],
    );

    const _isDateSelected = (date: Date) => isStartDate(date, selectedDate);

    const _isDateBlocked = (date: Date) =>
        isDateBlocked({
            date,
            minBookingDate,
            maxBookingDate,
            startDate: selectedDate,
            endDate: selectedDate,
            minBookingDays: 1,
            isDateBlockedFn: disabledDatesByUser,
        });

    const isDateFocused = (date: Date) => (focusedDate ? dayjsEn(date).isSame(focusedDate, "day") : false);

    const _isDateHovered = (date: Date) =>
        isDateHovered({
            date,
            hoveredDate,
            startDate: selectedDate,
            endDate: selectedDate,
            minBookingDays: 1,
            exactMinBookingDays: false,
            isDateBlocked: disabledDatesByUser,
        });

    useEffect(() => {
        if (!isBrowser) {
            return;
        }

        function handleKeyDown(e: KeyboardEvent) {
            if (
                (e.key === "ArrowRight" || e.key === "ArrowLeft" || e.key === "ArrowDown" || e.key === "ArrowUp") &&
                !focusedDate
            ) {
                const activeMonth = activeMonths[0];
                onDateFocus(activeMonth.date);
                setActiveMonths(getInitialMonths(numberOfMonths, activeMonth.date));
            }
        }
        window.addEventListener("keydown", handleKeyDown);

        return () => {
            window.removeEventListener("keydown", handleKeyDown);
        };
    }, [activeMonths, focusedDate, numberOfMonths, onDateFocus]);

    const onReset = useCallback(() => {
        onDateChange(null);
    }, [onDateChange]);

    function onDateSelect(date: Date) {
        if (!dayjsEn(date).isSame(selectedDate)) {
            onDateChange(date);
        } else {
            onReset();
        }
    }

    function onDateHover(date: Date | null) {
        if (!date) {
            setHoveredDate(null);
        } else {
            const isNotBlocked = !_isDateBlocked(date) || (selectedDate && dayjsEn(date).isSame(selectedDate, "day"));

            if (isNotBlocked) {
                setHoveredDate(date);
            } else if (hoveredDate !== null) {
                setHoveredDate(null);
            }
        }
    }

    return {
        ...useDatePickerNavigation({
            activeMonths,
            maxBookingDate,
            minBookingDate,
            numberOfMonths,
            setActiveMonths,
            setFocusedDate,
        }),
        activeMonths,
        firstDayOfWeek,
        focusedDate,
        hoveredDate,
        isDateBlocked: _isDateBlocked,
        isDateFocused,
        isDateHovered: _isDateHovered,
        isDateSelected: _isDateSelected,
        numberOfMonths,
        onDateFocus,
        onDateHover,
        onDateSelect,
        onResetDates: onReset,
    };
}
