import { useMemo } from "react";
import { CalendarDay, getDays, GetDaysProps } from "../utils/get-days";
import { getWeekdayLabels, GetWeekdayLabelsProps } from "../utils/get-weekday-labels";
import { dayjsEn } from "../utils/dayjs-en";

export const dayLabelFormatFn = (date: Date) => dayjsEn(date).format("DD");
export const weekdayLabelFormatFn = (date: Date) => dayjsEn(date).format("dd"); // localization
export const monthLabelFormatFn = (date: Date) => dayjsEn(date).format("MMMM YYYY");

export interface UseMonthReturnType {
    days: CalendarDay[];
    monthLabel: string;
    weekdayLabels: string[];
}

export interface UseMonthProps extends GetWeekdayLabelsProps, GetDaysProps {
    monthLabelFormat?: (date: Date) => string;
}

export function useMonth({
    dayLabelFormat = dayLabelFormatFn,
    firstDayOfWeek = 0,
    month,
    monthLabelFormat = monthLabelFormatFn,
    weekdayLabelFormat = weekdayLabelFormatFn,
    year,
}: UseMonthProps): UseMonthReturnType {
    const days = useMemo(
        () => getDays({ year, month, firstDayOfWeek, dayLabelFormat }),
        [year, month, firstDayOfWeek, dayLabelFormat],
    );
    const weekdayLabels = useMemo(
        () => getWeekdayLabels({ firstDayOfWeek, weekdayLabelFormat }),
        [firstDayOfWeek, weekdayLabelFormat],
    );

    return {
        days,
        monthLabel: monthLabelFormat(new Date(year, month)),
        weekdayLabels,
    };
}
