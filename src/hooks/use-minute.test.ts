import { act, renderHook } from "@testing-library/react-hooks";
import { useRef } from "react";
import { useMinute } from "./use-minute";

test("useMinute", () => {
    const { result } = renderHook(() =>
        useMinute({
            minute: 30,
            focusedMinute: null,
            isMinuteFocused: () => false,
            isMinuteSelected: () => false,
            onMinuteFocus: () => undefined,
            onMinuteSelect: () => undefined,
        }),
    );

    expect(result.current.tabIndex).toBe(0);
    expect(result.current.isSelected).toBe(false);
});

test("actions", () => {
    let selectedMinute: number | null = null;
    let focusedMinute: number | null = null;

    const { result: ref } = renderHook(() => useRef<HTMLElement>(null));

    const { result, rerender } = renderHook(() =>
        useMinute({
            minute: 30,
            focusedMinute: null,
            isMinuteFocused: minute => focusedMinute === minute,
            isMinuteSelected: minute => selectedMinute === minute,
            onMinuteFocus: minute => (focusedMinute = minute),
            onMinuteSelect: minute => (selectedMinute = minute),
            minuteRef: ref.current,
        }),
    );

    expect(result.current.tabIndex).toBe(0);

    // select
    expect(result.current.isSelected).toBe(false);
    act(() => {
        if (result.current.onClick) {
            result.current.onClick();
        }
        rerender();
    });
    expect(result.current.isSelected).toBe(true);
});
