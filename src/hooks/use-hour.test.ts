import { act, renderHook } from "@testing-library/react-hooks";
import { useRef } from "react";
import { useHour } from "./use-hour";

test("useHour", () => {
    const { result } = renderHook(() =>
        useHour({
            hour: 5,
            focusedHour: null,
            isHourFocused: () => false,
            isHourSelected: () => false,
            onHourFocus: () => undefined,
            onHourSelect: () => undefined,
        }),
    );

    expect(result.current.tabIndex).toBe(0);
    expect(result.current.isSelected).toBe(false);
});

test("actions", () => {
    let selectedHour: number | null = null;
    let focusedHour: number | null = null;

    const { result: ref } = renderHook(() => useRef<HTMLElement>(null));

    const { result, rerender } = renderHook(() =>
        useHour({
            hour: 30,
            focusedHour: null,
            isHourFocused: hour => focusedHour === hour,
            isHourSelected: hour => selectedHour === hour,
            onHourFocus: hour => (focusedHour = hour),
            onHourSelect: hour => (selectedHour = hour),
            hourRef: ref.current,
        }),
    );

    expect(result.current.tabIndex).toBe(0);

    // select
    expect(result.current.isSelected).toBe(false);
    act(() => {
        if (result.current.onClick) {
            result.current.onClick();
        }
        rerender();
    });
    expect(result.current.isSelected).toBe(true);
});
