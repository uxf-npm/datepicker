import { KeyboardEvent, RefObject, useCallback, useEffect } from "react";
import { dayjsEn } from "../utils/dayjs-en";

export interface UseDayProps {
    date: Date;
    dayRef?: RefObject<HTMLElement>;
    focusedDate: Date | null;
    isDateBlocked: (date: Date) => boolean;
    isDateFocused: (date: Date) => boolean;
    isDateHovered: (date: Date) => boolean;
    isDateInsideRange?: (date: Date) => boolean;
    isDateSelected: (date: Date) => boolean;
    onDateFocus: (date: Date) => void;
    onDateHover: (date: Date) => void;
    onDateSelect: (date: Date) => void;
}

export interface UseDayReturnType {
    disabledDate: boolean;
    isHovered: boolean;
    isSelected: boolean;
    isInsideRange: boolean;
    isToday: boolean;
    isWithinHoverRange: boolean;
    onClick?: () => void;
    onKeyDown: (e: KeyboardEvent<HTMLElement>) => void;
    onMouseEnter: () => void;
    tabIndex: number;
}

export function useDay({
    date,
    dayRef,
    focusedDate,
    isDateBlocked,
    isDateFocused,
    isDateHovered,
    isDateInsideRange,
    isDateSelected,
    onDateFocus,
    onDateHover,
    onDateSelect,
}: UseDayProps): UseDayReturnType {
    const onClick = useCallback(() => onDateSelect(date), [date, onDateSelect]);
    const onMouseEnter = useCallback(() => onDateHover(date), [date, onDateHover]);
    const disabled = isDateBlocked(date) && !isDateHovered(date);
    const isToday = dayjsEn().isSame(date, "day");

    useEffect(() => {
        if (dayRef && dayRef.current && isDateFocused(date)) {
            dayRef.current.focus();
        }
    }, [dayRef, date, isDateFocused]);

    const onKeyDown = useCallback(
        (e: KeyboardEvent<HTMLElement>) => {
            if (["ArrowRight", "ArrowLeft", "ArrowUp", "ArrowDown"].includes(e.key)) {
                e.preventDefault();
                switch (e.key) {
                    case "ArrowRight":
                        onDateFocus(dayjsEn(date).add(1, "day").toDate());
                        break;
                    case "ArrowLeft":
                        onDateFocus(dayjsEn(date).subtract(1, "day").toDate());
                        break;
                    case "ArrowUp":
                        onDateFocus(dayjsEn(date).subtract(7, "day").toDate());
                        break;
                    case "ArrowDown":
                        onDateFocus(dayjsEn(date).add(7, "day").toDate());
                        break;
                }
            }
        },
        [date, onDateFocus],
    );

    return {
        disabledDate: disabled,
        isHovered: isDateHovered(date),
        isSelected: isDateSelected(date),
        isInsideRange: isDateInsideRange ? isDateInsideRange(date) : false,
        isWithinHoverRange: isDateHovered(date),
        onClick: !disabled ? onClick : undefined,
        onKeyDown,
        onMouseEnter,
        tabIndex: focusedDate === null || isDateFocused(date) ? 0 : -1,
        isToday,
    };
}
