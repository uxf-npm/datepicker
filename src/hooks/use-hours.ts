import { useMemo } from "react";

export const labelFormatFn = (hour: number) => hour.toString().padStart(2, "0");

export type HourType = {
    value: number;
    label: string;
};

export interface UseHoursProps {
    labelFormat?: (hour: number) => string;
}

export interface UseHoursReturnType {
    hours: HourType[];
}

export function useHours({ labelFormat = labelFormatFn }: UseHoursProps): UseHoursReturnType {
    const hours = useMemo(
        () =>
            Array(24)
                .fill(0)
                .map((_, i) => ({ value: i, label: labelFormat(i) })),
        [labelFormat],
    );

    return {
        hours,
    };
}
