import { renderHook } from "@testing-library/react-hooks";
import dayjs from "dayjs";
import { useMonth } from "./use-month";

test("useMonth", () => {
    const { result } = renderHook(() =>
        useMonth({
            year: 2021,
            month: 0,
        }),
    );

    expect(result.current.monthLabel).toBe("January 2021");
    expect(result.current.days).toStrictEqual([
        { dayLabel: "27", date: new Date("2020-12-27"), currentMonth: false },
        { dayLabel: "28", date: new Date("2020-12-28"), currentMonth: false },
        { dayLabel: "29", date: new Date("2020-12-29"), currentMonth: false },
        { dayLabel: "30", date: new Date("2020-12-30"), currentMonth: false },
        { dayLabel: "31", date: new Date("2020-12-31"), currentMonth: false },
        { dayLabel: "01", date: new Date("2021-01-01"), currentMonth: true },
        { dayLabel: "02", date: new Date("2021-01-02"), currentMonth: true },
        { dayLabel: "03", date: new Date("2021-01-03"), currentMonth: true },
        { dayLabel: "04", date: new Date("2021-01-04"), currentMonth: true },
        { dayLabel: "05", date: new Date("2021-01-05"), currentMonth: true },
        { dayLabel: "06", date: new Date("2021-01-06"), currentMonth: true },
        { dayLabel: "07", date: new Date("2021-01-07"), currentMonth: true },
        { dayLabel: "08", date: new Date("2021-01-08"), currentMonth: true },
        { dayLabel: "09", date: new Date("2021-01-09"), currentMonth: true },
        { dayLabel: "10", date: new Date("2021-01-10"), currentMonth: true },
        { dayLabel: "11", date: new Date("2021-01-11"), currentMonth: true },
        { dayLabel: "12", date: new Date("2021-01-12"), currentMonth: true },
        { dayLabel: "13", date: new Date("2021-01-13"), currentMonth: true },
        { dayLabel: "14", date: new Date("2021-01-14"), currentMonth: true },
        { dayLabel: "15", date: new Date("2021-01-15"), currentMonth: true },
        { dayLabel: "16", date: new Date("2021-01-16"), currentMonth: true },
        { dayLabel: "17", date: new Date("2021-01-17"), currentMonth: true },
        { dayLabel: "18", date: new Date("2021-01-18"), currentMonth: true },
        { dayLabel: "19", date: new Date("2021-01-19"), currentMonth: true },
        { dayLabel: "20", date: new Date("2021-01-20"), currentMonth: true },
        { dayLabel: "21", date: new Date("2021-01-21"), currentMonth: true },
        { dayLabel: "22", date: new Date("2021-01-22"), currentMonth: true },
        { dayLabel: "23", date: new Date("2021-01-23"), currentMonth: true },
        { dayLabel: "24", date: new Date("2021-01-24"), currentMonth: true },
        { dayLabel: "25", date: new Date("2021-01-25"), currentMonth: true },
        { dayLabel: "26", date: new Date("2021-01-26"), currentMonth: true },
        { dayLabel: "27", date: new Date("2021-01-27"), currentMonth: true },
        { dayLabel: "28", date: new Date("2021-01-28"), currentMonth: true },
        { dayLabel: "29", date: new Date("2021-01-29"), currentMonth: true },
        { dayLabel: "30", date: new Date("2021-01-30"), currentMonth: true },
        { dayLabel: "31", date: new Date("2021-01-31"), currentMonth: true },
        { dayLabel: "01", date: new Date("2021-02-01"), currentMonth: false },
        { dayLabel: "02", date: new Date("2021-02-02"), currentMonth: false },
        { dayLabel: "03", date: new Date("2021-02-03"), currentMonth: false },
        { dayLabel: "04", date: new Date("2021-02-04"), currentMonth: false },
        { dayLabel: "05", date: new Date("2021-02-05"), currentMonth: false },
        { dayLabel: "06", date: new Date("2021-02-06"), currentMonth: false },
    ]);
    expect(result.current.weekdayLabels).toStrictEqual(["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]);
});

test("useMonth with custom formats and firstDayOfWeek", () => {
    const { result } = renderHook(() =>
        useMonth({
            year: 2021,
            month: 0,
            firstDayOfWeek: 1,
            monthLabelFormat: date => dayjs(date).format("MM"),
            dayLabelFormat: date => dayjs(date).format("D"),
            weekdayLabelFormat: date => dayjs(date).format("dddd"),
        }),
    );

    expect(result.current.monthLabel).toBe("01");
    expect(result.current.days).toStrictEqual([
        { dayLabel: "28", date: new Date("2020-12-28"), currentMonth: false },
        { dayLabel: "29", date: new Date("2020-12-29"), currentMonth: false },
        { dayLabel: "30", date: new Date("2020-12-30"), currentMonth: false },
        { dayLabel: "31", date: new Date("2020-12-31"), currentMonth: false },
        { dayLabel: "1", date: new Date("2021-01-01"), currentMonth: true },
        { dayLabel: "2", date: new Date("2021-01-02"), currentMonth: true },
        { dayLabel: "3", date: new Date("2021-01-03"), currentMonth: true },
        { dayLabel: "4", date: new Date("2021-01-04"), currentMonth: true },
        { dayLabel: "5", date: new Date("2021-01-05"), currentMonth: true },
        { dayLabel: "6", date: new Date("2021-01-06"), currentMonth: true },
        { dayLabel: "7", date: new Date("2021-01-07"), currentMonth: true },
        { dayLabel: "8", date: new Date("2021-01-08"), currentMonth: true },
        { dayLabel: "9", date: new Date("2021-01-09"), currentMonth: true },
        { dayLabel: "10", date: new Date("2021-01-10"), currentMonth: true },
        { dayLabel: "11", date: new Date("2021-01-11"), currentMonth: true },
        { dayLabel: "12", date: new Date("2021-01-12"), currentMonth: true },
        { dayLabel: "13", date: new Date("2021-01-13"), currentMonth: true },
        { dayLabel: "14", date: new Date("2021-01-14"), currentMonth: true },
        { dayLabel: "15", date: new Date("2021-01-15"), currentMonth: true },
        { dayLabel: "16", date: new Date("2021-01-16"), currentMonth: true },
        { dayLabel: "17", date: new Date("2021-01-17"), currentMonth: true },
        { dayLabel: "18", date: new Date("2021-01-18"), currentMonth: true },
        { dayLabel: "19", date: new Date("2021-01-19"), currentMonth: true },
        { dayLabel: "20", date: new Date("2021-01-20"), currentMonth: true },
        { dayLabel: "21", date: new Date("2021-01-21"), currentMonth: true },
        { dayLabel: "22", date: new Date("2021-01-22"), currentMonth: true },
        { dayLabel: "23", date: new Date("2021-01-23"), currentMonth: true },
        { dayLabel: "24", date: new Date("2021-01-24"), currentMonth: true },
        { dayLabel: "25", date: new Date("2021-01-25"), currentMonth: true },
        { dayLabel: "26", date: new Date("2021-01-26"), currentMonth: true },
        { dayLabel: "27", date: new Date("2021-01-27"), currentMonth: true },
        { dayLabel: "28", date: new Date("2021-01-28"), currentMonth: true },
        { dayLabel: "29", date: new Date("2021-01-29"), currentMonth: true },
        { dayLabel: "30", date: new Date("2021-01-30"), currentMonth: true },
        { dayLabel: "31", date: new Date("2021-01-31"), currentMonth: true },
    ]);
    expect(result.current.weekdayLabels).toStrictEqual([
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ]);
});
