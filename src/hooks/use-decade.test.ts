import { renderHook } from "@testing-library/react-hooks";
import dayjs from "dayjs";
import { useDecade } from "./use-decade";

test("useDecade", () => {
    const { result } = renderHook(() =>
        useDecade({
            year: 2021,
        }),
    );

    expect(result.current.decadeLabel).toBe("2017 – 2025");
    expect(result.current.years).toStrictEqual([
        { date: new Date("2017-01-01"), yearLabel: "2017" },
        { date: new Date("2018-01-01"), yearLabel: "2018" },
        { date: new Date("2019-01-01"), yearLabel: "2019" },
        { date: new Date("2020-01-01"), yearLabel: "2020" },
        { date: new Date("2021-01-01"), yearLabel: "2021" },
        { date: new Date("2022-01-01"), yearLabel: "2022" },
        { date: new Date("2023-01-01"), yearLabel: "2023" },
        { date: new Date("2024-01-01"), yearLabel: "2024" },
        { date: new Date("2025-01-01"), yearLabel: "2025" },
    ]);
});

test("useDecade with custom formats", () => {
    const { result } = renderHook(() =>
        useDecade({
            year: 2021,
            decadeLabelFormat: (start, end) => `${dayjs(start).format("YY")} - ${dayjs(end).format("YY")}`,
            yearLabelFormat: date => dayjs(date).format("YY"),
        }),
    );

    expect(result.current.decadeLabel).toBe("17 - 25");
    expect(result.current.years).toStrictEqual([
        { date: new Date("2017-01-01"), yearLabel: "17" },
        { date: new Date("2018-01-01"), yearLabel: "18" },
        { date: new Date("2019-01-01"), yearLabel: "19" },
        { date: new Date("2020-01-01"), yearLabel: "20" },
        { date: new Date("2021-01-01"), yearLabel: "21" },
        { date: new Date("2022-01-01"), yearLabel: "22" },
        { date: new Date("2023-01-01"), yearLabel: "23" },
        { date: new Date("2024-01-01"), yearLabel: "24" },
        { date: new Date("2025-01-01"), yearLabel: "25" },
    ]);
});
