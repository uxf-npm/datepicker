import { KeyboardEvent, RefObject, useCallback, useEffect } from "react";

export interface UseMinuteProps {
    minute: number;
    minuteRef?: RefObject<HTMLElement>;
    focusedMinute: number | null;
    isMinuteFocused: (minute: number) => boolean;
    isMinuteSelected: (minute: number) => boolean;
    onMinuteFocus: (minute: number) => void;
    onMinuteSelect: (minute: number) => void;
}

export interface UseMinuteReturnType {
    isSelected: boolean;
    onClick?: () => void;
    onKeyDown: (e: KeyboardEvent<HTMLElement>) => void;
    tabIndex: number;
}

export function useMinute({
    minute,
    minuteRef,
    focusedMinute,
    isMinuteFocused,
    isMinuteSelected,
    onMinuteFocus,
    onMinuteSelect,
}: UseMinuteProps): UseMinuteReturnType {
    const onClick = useCallback(() => onMinuteSelect(minute), [minute, onMinuteSelect]);

    useEffect(() => {
        if (minuteRef && minuteRef.current && isMinuteFocused(minute)) {
            minuteRef.current.focus();
        }
    }, [minuteRef, minute, isMinuteFocused]);

    const onKeyDown = useCallback(
        (e: KeyboardEvent<HTMLElement>) => {
            if (["ArrowRight", "ArrowLeft", "ArrowUp", "ArrowDown"].includes(e.key)) {
                e.preventDefault();
                switch (e.key) {
                    case "ArrowRight":
                        onMinuteFocus(minute + 5);
                        break;
                    case "ArrowLeft":
                        onMinuteFocus(minute - 5);
                        break;
                    case "ArrowUp":
                        onMinuteFocus(minute - 20);
                        break;
                    case "ArrowDown":
                        onMinuteFocus(minute + 20);
                        break;
                }
            }
        },
        [minute, onMinuteFocus],
    );

    return {
        isSelected: isMinuteSelected(minute),
        onClick: onClick,
        onKeyDown,
        tabIndex: focusedMinute === null || isMinuteFocused(minute) ? 0 : -1,
    };
}
