import { useCallback, useState } from "react";

export type TimeType = { hour: number; minute: number };

export type OnTimeChangeType = TimeType | null;

export interface UseTimePickerProps {
    onSelectCallback?: () => void;
    onTimeChange: (data: OnTimeChangeType) => void;
    selectedTime: TimeType | null;
}

export interface UseTimePickerReturnType {
    onTimeSelect: (time: TimeType) => void;
    goToPrevHour: () => void;
    goToPrevMinute: () => void;
    goToNextHour: () => void;
    goToNextMinute: () => void;
    focusedHour: number | null;
    focusedMinute: number | null;
    isHourFocused: (hour: number) => boolean;
    isHourSelected: (hour: number) => boolean;
    isMinuteFocused: (minute: number) => boolean;
    isMinuteSelected: (minute: number) => boolean;
    onHourFocus: (hour: number) => void;
    onHourSelect: (hour: number) => void;
    onMinuteFocus: (minute: number) => void;
    onMinuteSelect: (minute: number) => void;
}

export function useTimePicker({
    selectedTime,
    onTimeChange,
    onSelectCallback,
}: UseTimePickerProps): UseTimePickerReturnType {
    const [focusedHour, setFocusedHour] = useState<number | null>(selectedTime?.hour ?? null);
    const [focusedMinute, setFocusedMinute] = useState<number | null>(selectedTime?.minute ?? null);

    const _isTimeSelected = (time: TimeType) => time.hour === selectedTime?.hour && time.minute === selectedTime.minute;

    function onTimeSelect(time: TimeType) {
        if (!_isTimeSelected(time)) {
            onTimeChange(time);
        }
    }

    const isHourFocused = (hour: number) => hour === focusedHour;
    const isMinuteFocused = (minute: number) => minute === focusedMinute;

    const isHourSelected = (hour: number) => hour === selectedTime?.hour;
    const isMinuteSelected = (minute: number) => minute === selectedTime?.minute;

    const onHourFocus = (hour: number) => {
        setFocusedHour(hour);
    };
    const onMinuteFocus = (minute: number) => {
        setFocusedMinute(minute);
    };

    const onHourSelect = (hour: number) => {
        onTimeChange({ hour, minute: selectedTime?.minute ?? 0 });
        onSelectCallback?.();
    };
    const onMinuteSelect = (minute: number) => {
        onTimeChange({ hour: selectedTime?.hour ?? 0, minute });
        onSelectCallback?.();
    };

    const goToPrevHour = useCallback(() => {
        const nowHour = selectedTime?.hour ?? 0;
        const nowMinute = selectedTime?.minute ?? 0;

        onTimeChange({ hour: nowHour === 0 ? 23 : nowHour - 1, minute: nowMinute });
    }, [onTimeChange, selectedTime]);

    const goToNextHour = useCallback(() => {
        const nowHour = selectedTime?.hour ?? 0;
        const nowMinute = selectedTime?.minute ?? 0;

        onTimeChange({ hour: nowHour === 23 ? 0 : nowHour + 1, minute: nowMinute });
    }, [onTimeChange, selectedTime]);

    const goToPrevMinute = useCallback(() => {
        const nowHour = selectedTime?.hour ?? 0;
        const nowMinute = selectedTime?.minute ?? 0;

        onTimeChange({
            hour: nowMinute === 0 ? (nowHour === 0 ? 23 : nowHour - 1) : nowHour,
            minute: nowMinute === 0 ? 59 : nowMinute - 1,
        });
    }, [onTimeChange, selectedTime]);

    const goToNextMinute = useCallback(() => {
        const nowHour = selectedTime?.hour ?? 0;
        const nowMinute = selectedTime?.minute ?? 0;

        onTimeChange({
            hour: nowMinute === 59 ? (nowHour === 23 ? 0 : nowHour + 1) : nowHour,
            minute: nowMinute === 59 ? 0 : nowMinute + 1,
        });
    }, [onTimeChange, selectedTime]);

    return {
        goToPrevHour,
        goToNextHour,
        goToPrevMinute,
        goToNextMinute,
        onTimeSelect,
        focusedHour,
        focusedMinute,
        isHourFocused,
        isHourSelected,
        isMinuteFocused,
        isMinuteSelected,
        onHourFocus,
        onHourSelect,
        onMinuteFocus,
        onMinuteSelect,
    };
}
