import { useMemo } from "react";
import { CalendarMonth, getMonths, GetMonthsProps } from "../utils/get-months";
import { dayjsEn } from "../utils/dayjs-en";

export const monthLabelFormatFn = (date: Date) => dayjsEn(date).format("MMMM"); // localized
export const yearLabelFormatFn = (date: Date) => dayjsEn(date).format("YYYY");

export interface UseYearReturnType {
    months: CalendarMonth[];
    yearLabel: string;
}

export interface UseYearProps extends GetMonthsProps {
    yearLabelFormat?: (date: Date) => string;
    monthLabelFormat?: (date: Date) => string;
}

export function useYear({
    yearLabelFormat = yearLabelFormatFn,
    monthLabelFormat = monthLabelFormatFn,
    year,
}: UseYearProps): UseYearReturnType {
    const months = useMemo(() => getMonths({ year, monthLabelFormat }), [year, monthLabelFormat]);

    return {
        months,
        yearLabel: yearLabelFormat(new Date(year, 0)),
    };
}
