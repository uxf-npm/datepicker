import { Dispatch, SetStateAction, useCallback } from "react";
import { getInitialMonths } from "../utils/get-initial-months";
import { getNextActiveMonth } from "../utils/get-next-active-month";
import { MonthType } from "../utils/types";
import { dayjsEn } from "../utils/dayjs-en";

export interface UseDatePickerNavigationProps {
    activeMonths: MonthType[];
    setActiveMonths: Dispatch<SetStateAction<MonthType[]>>;
    setFocusedDate: Dispatch<SetStateAction<Date | null>>;
    minBookingDate?: Date;
    maxBookingDate?: Date;
    numberOfMonths: number;
}

export interface UseDatePickerNavigationReturnType {
    canGoToMonth: (month: Date) => boolean;
    canGoToNextMonth: boolean;
    canGoToNextYear: boolean;
    canGoToPrevMonth: boolean;
    canGoToPrevYear: boolean;
    canGoToYear: (month: Date) => boolean;
    goToDate: (date: Date) => void;
    goToNextMonths: () => void;
    goToNextMonthsByOneMonth: () => void;
    goToNextYear: (numYears?: number) => void;
    goToPrevMonths: () => void;
    goToPrevMonthsByOneMonth: () => void;
    goToPrevYear: (numYears?: number) => void;
}

export function useDatePickerNavigation({
    activeMonths,
    setActiveMonths,
    setFocusedDate,
    minBookingDate,
    maxBookingDate,
    numberOfMonths,
}: UseDatePickerNavigationProps): UseDatePickerNavigationReturnType {
    const goToPrevMonths = useCallback(() => {
        setActiveMonths(getNextActiveMonth(activeMonths, numberOfMonths, -1));
        setFocusedDate(null);
    }, [activeMonths, numberOfMonths, setActiveMonths, setFocusedDate]);

    const goToPrevMonthsByOneMonth = useCallback(() => {
        setActiveMonths(getNextActiveMonth(activeMonths, numberOfMonths, -1, 1));
        setFocusedDate(null);
    }, [activeMonths, numberOfMonths, setActiveMonths, setFocusedDate]);

    const goToNextMonths = useCallback(() => {
        setActiveMonths(getNextActiveMonth(activeMonths, numberOfMonths, 1));
        setFocusedDate(null);
    }, [activeMonths, numberOfMonths, setActiveMonths, setFocusedDate]);

    const goToNextMonthsByOneMonth = useCallback(() => {
        setActiveMonths(getNextActiveMonth(activeMonths, numberOfMonths, 1, 1));
        setFocusedDate(null);
    }, [activeMonths, numberOfMonths, setActiveMonths, setFocusedDate]);

    const goToDate = useCallback(
        (date: Date) => {
            setActiveMonths(getInitialMonths(numberOfMonths, date));
            setFocusedDate(null);
        },
        [numberOfMonths, setActiveMonths, setFocusedDate],
    );

    const goToPrevYear = useCallback(
        (numYears = 1) => {
            setActiveMonths(getNextActiveMonth(activeMonths, numberOfMonths, -(numYears * 12 - numberOfMonths + 1)));
            setFocusedDate(null);
        },
        [activeMonths, numberOfMonths, setActiveMonths, setFocusedDate],
    );

    const goToNextYear = useCallback(
        (numYears = 1) => {
            setActiveMonths(getNextActiveMonth(activeMonths, numberOfMonths, numYears * 12 - numberOfMonths + 1));
            setFocusedDate(null);
        },
        [activeMonths, numberOfMonths, setActiveMonths, setFocusedDate],
    );

    const canGoToPrevMonth = minBookingDate ? !dayjsEn(activeMonths[0].date).isBefore(dayjsEn(minBookingDate)) : true;

    const canGoToNextMonth = maxBookingDate
        ? !dayjsEn(activeMonths[activeMonths.length - 1].date)
              .endOf("month")
              .isAfter(dayjsEn(maxBookingDate))
        : true;

    const canGoToPrevYear = minBookingDate
        ? !dayjsEn(activeMonths[0].date).startOf("year").isBefore(dayjsEn(minBookingDate))
        : true;

    const canGoToNextYear = maxBookingDate
        ? !dayjsEn(activeMonths[activeMonths.length - 1].date)
              .endOf("year")
              .isAfter(dayjsEn(maxBookingDate))
        : true;

    const canGoTo = useCallback(
        (month: Date, type: "year" | "month"): boolean => {
            if (minBookingDate || maxBookingDate) {
                if (minBookingDate && maxBookingDate) {
                    return (
                        dayjsEn(month).endOf(type).add(1, "day").isAfter(minBookingDate, "day") &&
                        dayjsEn(month).startOf(type).subtract(1, "day").isBefore(maxBookingDate, "day")
                    );
                }
                if (minBookingDate) {
                    return dayjsEn(month).endOf(type).add(1, "day").isAfter(minBookingDate, "day");
                }
                if (maxBookingDate) {
                    return dayjsEn(month).startOf(type).subtract(1, "day").isBefore(maxBookingDate, "day");
                }
            }

            return true;
        },
        [maxBookingDate, minBookingDate],
    );

    const canGoToMonth = useCallback((date: Date): boolean => canGoTo(date, "month"), [canGoTo]);

    const canGoToYear = useCallback((date: Date): boolean => canGoTo(date, "year"), [canGoTo]);

    return {
        canGoToMonth,
        canGoToNextMonth,
        canGoToNextYear,
        canGoToPrevMonth,
        canGoToPrevYear,
        canGoToYear,
        goToDate,
        goToNextMonths,
        goToNextMonthsByOneMonth,
        goToNextYear,
        goToPrevMonths,
        goToPrevMonthsByOneMonth,
        goToPrevYear,
    };
}
