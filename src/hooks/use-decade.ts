import { useMemo } from "react";
import { CalendarYear, getYears } from "../utils/get-years";
import { dayjsEn } from "../utils/dayjs-en";

export const yearLabelFormatFn = (date: Date) => dayjsEn(date).format("YYYY");
export const decadeLabelFormatFn = (start: Date, end: Date) =>
    `${dayjsEn(start).format("YYYY")} \u2013 ${dayjsEn(end).format("YYYY")}`;

export interface UseDecadeReturnType {
    years: CalendarYear[];
    decadeLabel: string;
}

export interface UseDecadeProps {
    decadeLabelFormat?: (start: Date, end: Date) => string;
    year: number;
    yearLabelFormat?: (date: Date) => string;
}

export function useDecade({
    yearLabelFormat = yearLabelFormatFn,
    decadeLabelFormat = decadeLabelFormatFn,
    year,
}: UseDecadeProps): UseDecadeReturnType {
    const currentYear = new Date(year, 0);
    const startYear = dayjsEn(currentYear).subtract(4, "years").startOf("year").toDate();
    const endYear = dayjsEn(currentYear).add(4, "years").endOf("year").toDate();
    const years = useMemo(
        () => getYears({ startYear, endYear, yearLabelFormat }),
        [endYear, startYear, yearLabelFormat],
    );

    return {
        years,
        decadeLabel: decadeLabelFormat(startYear, endYear),
    };
}
