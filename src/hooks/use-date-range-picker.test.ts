import { renderHook, act } from "@testing-library/react-hooks";
import { useState } from "react";
import { OnDatesChangeProps, useDateRangePicker } from "./use-date-range-picker";

beforeAll(() => {
    jest.useFakeTimers("modern");
    jest.setSystemTime(new Date("2021-09-25"));
});

test("base data", () => {
    const { result } = renderHook(() =>
        useDateRangePicker({
            startDate: null,
            endDate: null,
            onDatesChange: () => undefined,
            focusedInput: "startDate",
            minBookingDate: new Date("2020-01-01"),
        }),
    );

    expect(result.current.firstDayOfWeek).toBe(0);
    expect(result.current.focusedDate).toBe(null);
    expect(result.current.hoveredDate).toBe(null);
    expect(result.current.activeMonths).toStrictEqual([
        {
            date: new Date("2021-09-01"),
            month: 8,
            year: 2021,
        },
        {
            date: new Date("2021-10-01"),
            month: 9,
            year: 2021,
        },
    ]);
    expect(result.current.isDateBlocked(new Date("2020-01-01"))).toBe(false);
    expect(result.current.isDateBlocked(new Date("2019-12-31"))).toBe(true);
});

test("date focus", () => {
    const BASE_DATE = new Date("2021-09-15");
    const { result: state } = renderHook(() =>
        useState<OnDatesChangeProps>({ startDate: null, endDate: null, focusedInput: "startDate" }),
    );
    const { result } = renderHook(() =>
        useDateRangePicker({
            startDate: state.current[0].startDate,
            endDate: state.current[0].endDate,
            onDatesChange: data => state.current[1](data),
            focusedInput: state.current[0].focusedInput,
            minBookingDate: new Date("2020-01-01"),
        }),
    );

    const NEW_DATE = new Date("2022-12-22");
    expect(result.current.isDateFocused(BASE_DATE)).toBe(false);
    expect(result.current.isDateFocused(NEW_DATE)).toBe(false);

    act(() => {
        result.current.onDateFocus(NEW_DATE);
    });

    expect(result.current.isDateFocused(NEW_DATE)).toBe(true);
    expect(result.current.isDateFocused(BASE_DATE)).toBe(false);
});

test("date select", () => {
    const BASE_DATE = new Date("2021-09-15");
    const { result: state } = renderHook(() =>
        useState<OnDatesChangeProps>({ startDate: null, endDate: null, focusedInput: "startDate" }),
    );
    const { result, rerender } = renderHook(() =>
        useDateRangePicker({
            startDate: state.current[0].startDate,
            endDate: state.current[0].endDate,
            onDatesChange: data => state.current[1](data),
            focusedInput: state.current[0].focusedInput,
            minBookingDate: new Date("2020-01-01"),
        }),
    );

    const NEW_DATE = new Date("2022-12-22");
    expect(result.current.isDateSelected(BASE_DATE)).toBe(false);
    expect(result.current.isDateSelected(NEW_DATE)).toBe(false);

    act(() => {
        result.current.onDateSelect(NEW_DATE);
        rerender();
    });

    expect(result.current.isDateSelected(NEW_DATE)).toBe(true);
    expect(result.current.isDateSelected(BASE_DATE)).toBe(false);

    // select another
    act(() => {
        result.current.onDateSelect(BASE_DATE);
        rerender();
    });

    expect(result.current.isDateSelected(BASE_DATE)).toBe(true);

    // reset
    act(() => {
        result.current.onDateSelect(BASE_DATE);
        result.current.onResetDates();
        rerender();
    });

    expect(result.current.isDateSelected(BASE_DATE)).toBe(false);
});
