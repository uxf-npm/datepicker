import { act, renderHook } from "@testing-library/react-hooks";
import dayjs from "dayjs";
import { KeyboardEvent, useRef } from "react";
import { useDay } from "./use-day";

const e = { preventDefault: () => undefined, key: undefined } as unknown as KeyboardEvent<HTMLElement>;

beforeAll(() => {
    jest.useFakeTimers("modern");
    jest.setSystemTime(new Date("2021-09-06"));
});

test("useDay", () => {
    const { result } = renderHook(() =>
        useDay({
            date: new Date("2021-09-05"),
            isDateBlocked: () => false,
            focusedDate: null,
            onDateFocus: () => undefined,
            isDateFocused: () => false,
            isDateHovered: () => false,
            isDateSelected: () => false,
            isDateInsideRange: () => false,
            onDateHover: () => undefined,
            onDateSelect: () => undefined,
        }),
    );

    expect(result.current.tabIndex).toBe(0);
    expect(result.current.disabledDate).toBe(false);
    expect(result.current.isHovered).toBe(false);
    expect(result.current.isToday).toBe(false);
    expect(result.current.isSelected).toBe(false);
    expect(result.current.isInsideRange).toBe(false);
    expect(result.current.isWithinHoverRange).toBe(false);
});

test("actions", () => {
    let hoveredDate: Date | null = null;
    let selectedDate: Date | null = null;
    let focusedDate: Date | null = null;
    const insideRangeDate: Date | null = null;

    const { result: ref } = renderHook(() => useRef<HTMLElement>(null));

    const { result, rerender } = renderHook(() =>
        useDay({
            date: new Date("2021-09-05"),
            isDateBlocked: () => false,
            focusedDate: null,
            onDateFocus: date => (focusedDate = date),
            isDateFocused: date => dayjs(date).isSame(focusedDate, "day"),
            isDateHovered: date => dayjs(date).isSame(hoveredDate, "day"),
            isDateSelected: date => dayjs(date).isSame(selectedDate, "day"),
            isDateInsideRange: date => dayjs(date).isSame(insideRangeDate, "day"),
            onDateHover: date => (hoveredDate = date),
            onDateSelect: date => (selectedDate = date),
            dayRef: ref.current,
        }),
    );

    // hover
    expect(result.current.isHovered).toBe(false);
    act(() => {
        result.current.onMouseEnter();
        rerender();
    });
    expect(result.current.isHovered).toBe(true);
    expect(result.current.tabIndex).toBe(0);

    // select
    expect(result.current.isSelected).toBe(false);
    act(() => {
        if (result.current.onClick) {
            result.current.onClick();
        }
        rerender();
    });
    expect(result.current.isSelected).toBe(true);
    expect(result.current.isInsideRange).toBe(false);

    // keyDown
    act(() => {
        e.key = "ArrowRight";
        result.current.onKeyDown(e);
        e.key = "ArrowLeft";
        result.current.onKeyDown(e);
        e.key = "ArrowUp";
        result.current.onKeyDown(e);
        e.key = "ArrowDown";
        result.current.onKeyDown(e);
        e.key = "Enter";
        result.current.onKeyDown(e);
        rerender();
    });
    expect(result.current.isHovered).toBe(true);
});
