import { act, renderHook } from "@testing-library/react-hooks";
import { useState } from "react";
import { TimeType, useTimePicker } from "./use-time-picker";

test("base data", () => {
    const BASE_TIME: TimeType = { hour: 12, minute: 57 };
    const { result } = renderHook(() =>
        useTimePicker({
            selectedTime: BASE_TIME,
            onTimeChange: () => undefined,
        }),
    );

    expect(result.current.focusedHour).toBe(BASE_TIME.hour);
    expect(result.current.focusedMinute).toBe(BASE_TIME.minute);
});

test("time focus", () => {
    const BASE_TIME: TimeType = { hour: 12, minute: 57 };
    const { result: state } = renderHook(() => useState<TimeType | null>(BASE_TIME));
    const { result } = renderHook(() =>
        useTimePicker({ selectedTime: state.current[0], onTimeChange: time => state.current[1](time) }),
    );

    const NEW_TIME = { hour: 13, minute: 15 };
    expect(result.current.isHourFocused(BASE_TIME.hour)).toBe(true);
    expect(result.current.isHourFocused(NEW_TIME.hour)).toBe(false);
    expect(result.current.isMinuteFocused(BASE_TIME.minute)).toBe(true);
    expect(result.current.isMinuteFocused(NEW_TIME.minute)).toBe(false);

    act(() => {
        result.current.onHourFocus(NEW_TIME.hour);
        result.current.onMinuteFocus(NEW_TIME.minute);
    });

    expect(result.current.isHourFocused(NEW_TIME.hour)).toBe(true);
    expect(result.current.isHourFocused(BASE_TIME.hour)).toBe(false);
    expect(result.current.isMinuteFocused(NEW_TIME.minute)).toBe(true);
    expect(result.current.isMinuteFocused(BASE_TIME.minute)).toBe(false);
});

test("time select", () => {
    const BASE_TIME: TimeType = { hour: 12, minute: 57 };
    const { result: state } = renderHook(() => useState<TimeType | null>(BASE_TIME));
    const { result, rerender } = renderHook(() =>
        useTimePicker({ selectedTime: state.current[0], onTimeChange: time => state.current[1](time) }),
    );

    const NEW_TIME = { hour: 13, minute: 15 };
    expect(result.current.isHourSelected(BASE_TIME.hour)).toBe(true);
    expect(result.current.isHourSelected(NEW_TIME.hour)).toBe(false);
    expect(result.current.isMinuteSelected(BASE_TIME.minute)).toBe(true);
    expect(result.current.isMinuteSelected(NEW_TIME.minute)).toBe(false);

    act(() => {
        result.current.onTimeSelect(NEW_TIME);
        rerender();
    });

    expect(result.current.isHourSelected(NEW_TIME.hour)).toBe(true);
    expect(result.current.isHourSelected(BASE_TIME.hour)).toBe(false);
    expect(result.current.isMinuteSelected(NEW_TIME.minute)).toBe(true);
    expect(result.current.isMinuteSelected(BASE_TIME.minute)).toBe(false);
});

test("time navigation", () => {
    const BASE_TIME: TimeType = { hour: 1, minute: 58 };
    const { result: state } = renderHook(() => useState<TimeType | null>(BASE_TIME));
    const { result, rerender } = renderHook(() =>
        useTimePicker({ selectedTime: state.current[0], onTimeChange: time => state.current[1](time) }),
    );

    act(() => {
        result.current.goToPrevHour();
        rerender();
    });
    expect(state.current[0]).toStrictEqual({ hour: 0, minute: 58 });

    act(() => {
        result.current.goToNextMinute();
        rerender();
    });
    expect(state.current[0]).toStrictEqual({ hour: 0, minute: 59 });

    act(() => {
        result.current.goToPrevHour();
        rerender();
    });
    expect(state.current[0]).toStrictEqual({ hour: 23, minute: 59 });

    act(() => {
        result.current.goToNextHour();
        rerender();
    });
    expect(state.current[0]).toStrictEqual({ hour: 0, minute: 59 });

    act(() => {
        result.current.goToNextMinute();
        rerender();
    });
    expect(state.current[0]).toStrictEqual({ hour: 1, minute: 0 });

    act(() => {
        result.current.goToPrevMinute();
        rerender();
    });
    expect(state.current[0]).toStrictEqual({ hour: 0, minute: 59 });
});
