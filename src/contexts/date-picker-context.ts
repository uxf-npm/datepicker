import { createContext } from "react";
import { UseDatePickerReturnType } from "../hooks/use-date-picker";

export type DatePickerContextProps = UseDatePickerReturnType;

export const DatePickerContext = createContext<DatePickerContextProps>({
    activeMonths: [],
    canGoToMonth: () => true,
    canGoToNextMonth: true,
    canGoToNextYear: true,
    canGoToPrevMonth: true,
    canGoToPrevYear: true,
    canGoToYear: () => true,
    firstDayOfWeek: 0,
    focusedDate: null,
    goToDate: () => undefined,
    goToNextMonths: () => undefined,
    goToNextMonthsByOneMonth: () => undefined,
    goToNextYear: () => undefined,
    goToPrevMonths: () => undefined,
    goToPrevMonthsByOneMonth: () => undefined,
    goToPrevYear: () => undefined,
    hoveredDate: null,
    isDateBlocked: () => false,
    isDateFocused: () => false,
    isDateHovered: () => false,
    isDateSelected: () => false,
    numberOfMonths: 1,
    onDateFocus: () => undefined,
    onDateHover: () => undefined,
    onDateSelect: () => undefined,
    onResetDates: () => undefined,
});
