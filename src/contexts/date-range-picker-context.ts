import { createContext } from "react";
import { UseDateRangePickerReturnType } from "../hooks/use-date-range-picker";

export type DatePickerContextProps = UseDateRangePickerReturnType;

export const DateRangePickerContext = createContext<DatePickerContextProps>({
    activeMonths: [],
    canGoToMonth: () => true,
    canGoToNextMonth: true,
    canGoToNextYear: true,
    canGoToPrevMonth: true,
    canGoToPrevYear: true,
    canGoToYear: () => true,
    firstDayOfWeek: 0,
    focusedDate: null,
    goToDate: () => undefined,
    goToNextMonths: () => undefined,
    goToNextMonthsByOneMonth: () => undefined,
    goToNextYear: () => undefined,
    goToPrevMonths: () => undefined,
    goToPrevMonthsByOneMonth: () => undefined,
    goToPrevYear: () => undefined,
    hoveredDate: null,
    isDateBlocked: () => false,
    isDateFocused: () => false,
    isDateHovered: () => false,
    isDateInsideRange: () => false,
    isDateSelected: () => false,
    isEndDate: () => false,
    isStartDate: () => false,
    numberOfMonths: 2,
    onDateFocus: () => undefined,
    onDateHover: () => undefined,
    onDateSelect: () => undefined,
    onResetDates: () => undefined,
});
