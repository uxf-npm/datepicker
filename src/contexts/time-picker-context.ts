import { createContext } from "react";

export type TimePickerContextProps = {
    focusedHour: number | null;
    focusedMinute: number | null;
    isHourFocused: (hour: number) => boolean;
    isHourSelected: (hour: number) => boolean;
    isMinuteFocused: (minute: number) => boolean;
    isMinuteSelected: (minute: number) => boolean;
    onHourFocus: (hour: number) => void;
    onHourSelect: (hour: number) => void;
    onMinuteFocus: (minute: number) => void;
    onMinuteSelect: (minute: number) => void;
};

export const TimePickerContext = createContext<TimePickerContextProps>({
    focusedHour: null,
    focusedMinute: null,
    isHourFocused: () => false,
    isHourSelected: () => false,
    isMinuteFocused: () => false,
    isMinuteSelected: () => false,
    onHourFocus: () => undefined,
    onHourSelect: () => undefined,
    onMinuteFocus: () => undefined,
    onMinuteSelect: () => undefined,
});
