import dayjs, { ConfigType, extend } from "dayjs";
import "dayjs/locale/en";
import isBetween from "dayjs/plugin/isBetween";

extend(isBetween);

export const dayjsEn = (value?: ConfigType) => dayjs(value).locale("en");
