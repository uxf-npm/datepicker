import { isDateBlocked } from "./is-date-blocked";

test("isDateBlocked", () => {
    // custom fn
    expect(
        isDateBlocked({
            startDate: new Date("2021-08-08"),
            endDate: null,
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            date: new Date("2021-08-08"),
            unavailableDates: undefined,
            isDateBlockedFn: () => true,
        }),
    ).toBe(true);

    // start day only
    expect(
        isDateBlocked({
            startDate: new Date("2021-08-08"),
            endDate: null,
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            date: new Date("2021-08-08"),
            unavailableDates: undefined,
            isDateBlockedFn: undefined,
        }),
    ).toBe(false);

    // before minBookingDate
    expect(
        isDateBlocked({
            startDate: null,
            endDate: null,
            minBookingDays: 1,
            minBookingDate: new Date("2021-08-10"),
            maxBookingDate: undefined,
            date: new Date("2021-08-08"),
            unavailableDates: undefined,
            isDateBlockedFn: undefined,
        }),
    ).toBe(true);

    // after maxBookingDate
    expect(
        isDateBlocked({
            startDate: null,
            endDate: null,
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: new Date("2021-08-06"),
            date: new Date("2021-08-08"),
            unavailableDates: undefined,
            isDateBlockedFn: undefined,
        }),
    ).toBe(true);

    // in unavailable dates
    expect(
        isDateBlocked({
            startDate: null,
            endDate: null,
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            date: new Date("2021-08-08"),
            unavailableDates: [new Date("2021-08-08")],
            isDateBlockedFn: undefined,
        }),
    ).toBe(true);
});
