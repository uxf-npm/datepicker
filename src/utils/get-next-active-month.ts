import { getDateMonthAndYear } from "./get-date-month-and-year";
import { MonthType } from "./types";
import { dayjsEn } from "./dayjs-en";

export function getNextActiveMonth(
    activeMonth: MonthType[],
    numberOfMonths: number,
    counter: number,
    step?: number,
): MonthType[] {
    let prevMonth;

    if (step) {
        prevMonth = counter > 0 ? 0 : activeMonth.length - step;
    } else {
        prevMonth = counter > 0 ? activeMonth.length - 1 : 0;
    }

    let prevMonthDate = activeMonth[prevMonth].date;

    return Array.from(Array(numberOfMonths).keys()).reduce((m: MonthType[]) => {
        if (m.length === 0) {
            prevMonthDate = dayjsEn(prevMonthDate).add(counter, "month").toDate();
        } else {
            prevMonthDate = dayjsEn(prevMonthDate)
                .add(counter >= 0 ? 1 : -1, "month")
                .toDate();
        }
        return counter > 0
            ? m.concat([getDateMonthAndYear(prevMonthDate)])
            : [getDateMonthAndYear(prevMonthDate)].concat(m);
    }, []);
}
