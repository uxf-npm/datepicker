import { getCurrentYearMonthAndDate } from "./get-current-year-month-and-date";

beforeAll(() => {
    jest.useFakeTimers("modern");
});

test("getCurrentYearMonthAndDate", () => {
    jest.setSystemTime(new Date("2021-08-10"));
    expect(getCurrentYearMonthAndDate()).toStrictEqual({ year: 2021, month: 7, date: new Date("2021-08-01") });

    jest.setSystemTime(new Date("2020-02-15"));
    expect(getCurrentYearMonthAndDate()).toStrictEqual({ year: 2020, month: 1, date: new Date("2020-02-01") });
});
