import { dayjsEn } from "./dayjs-en";

export function isInUnavailableDates(unavailableDates: Date[] = [], date: Date) {
    return unavailableDates.some(_date => dayjsEn(date).isSame(_date, "day"));
}
