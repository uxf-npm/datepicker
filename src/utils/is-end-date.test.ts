import { isEndDate } from "./is-end-date";

test("isEndDate", () => {
    expect(isEndDate(new Date("2020-08-08"), new Date("2020-08-08"))).toBe(true);
    expect(isEndDate(new Date("2021-12-15"), new Date("2021-12-15"))).toBe(true);
    expect(isEndDate(new Date("1999-01-05"), new Date("1999-01-05"))).toBe(true);

    expect(isEndDate(new Date("2020-08-09"), new Date("2020-08-08"))).toBe(false);
    expect(isEndDate(new Date("2020-08-10"), new Date("2020-08-08"))).toBe(false);
    expect(isEndDate(new Date("2021-08-08"), new Date("2020-08-08"))).toBe(false);
});
