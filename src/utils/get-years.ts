import { getEach } from "./get-each";
import { dayjsEn } from "./dayjs-en";

export interface GetYearsProps {
    startYear: Date;
    endYear: Date;
    yearLabelFormat?(date: Date): string;
}

export type CalendarYear = { yearLabel: string; date: Date };

export function getYears({
    startYear,
    endYear,
    yearLabelFormat = (date: Date) => dayjsEn(date).format("YYYY"),
}: GetYearsProps): CalendarYear[] {
    return getEach(startYear, endYear, "year").map(d => ({
        date: d,
        yearLabel: yearLabelFormat(d),
    }));
}
