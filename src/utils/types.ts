export type FirstDayOfWeek = 0 | 1 | 2 | 3 | 4 | 5 | 6;

export interface MonthType {
    year: number;
    month: number;
    date: Date;
}
