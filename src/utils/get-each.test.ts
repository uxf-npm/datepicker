import { getEach } from "./get-each";

test("getEach day", () => {
    expect(getEach(new Date("2021-05-05"), new Date("2021-05-07"), "day")).toStrictEqual([
        new Date("2021-05-05"),
        new Date("2021-05-06"),
        new Date("2021-05-07"),
    ]);
});

test("getEach month", () => {
    expect(getEach(new Date("2021-05-05"), new Date("2021-08-07"), "month")).toStrictEqual([
        new Date("2021-05-01"),
        new Date("2021-06-01"),
        new Date("2021-07-01"),
        new Date("2021-08-01"),
    ]);
});

test("getEach year", () => {
    expect(getEach(new Date("2015-05-05"), new Date("2020-05-07"), "year")).toStrictEqual([
        new Date("2015-01-01"),
        new Date("2016-01-01"),
        new Date("2017-01-01"),
        new Date("2018-01-01"),
        new Date("2019-01-01"),
        new Date("2020-01-01"),
    ]);
});

test("getEach invalid input", () => {
    expect(() => getEach(new Date("2015-05-05"), new Date("2014-05-07"), "day")).toThrowError();
});
