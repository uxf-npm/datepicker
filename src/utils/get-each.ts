import { dayjsEn } from "./dayjs-en";

export function getEach(
    start: Date,
    end: Date,
    type: "day" | "month" | "year",
    arr = [dayjsEn(start).startOf(type).toDate()],
): Date[] {
    if (dayjsEn(start).isAfter(end)) {
        throw new Error("start must precede end");
    }

    const next = dayjsEn(start).add(1, type).startOf(type).toDate();

    if (dayjsEn(next).isAfter(end, type)) {
        return arr;
    }
    return getEach(next, end, type, arr.concat(next));
}
