import { MonthType } from "./types";
import { dayjsEn } from "./dayjs-en";

export function getDateMonthAndYear(date: Date): MonthType {
    const day = dayjsEn(date).startOf("month").toDate();
    const year = dayjsEn(day).year();
    const month = dayjsEn(day).month();
    return {
        year,
        month,
        date: day,
    };
}
