import { getEach } from "./get-each";
import { dayjsEn } from "./dayjs-en";

export interface CanSelectRangeProps {
    startDate: Date;
    endDate: Date | null;
    isDateBlocked(date: Date): boolean;
    minBookingDays: number;
    exactMinBookingDays?: boolean;
    minBookingDate?: Date;
    maxBookingDate?: Date;
}

// eslint-disable-next-line complexity
export function canSelectRange({
    startDate,
    endDate,
    isDateBlocked,
    minBookingDays,
    exactMinBookingDays,
    minBookingDate,
    maxBookingDate,
}: CanSelectRangeProps) {
    const isStartDateAfterOrEqualMinDate = minBookingDate
        ? !dayjsEn(startDate).isBefore(dayjsEn(minBookingDate).subtract(1, "day"))
        : true;
    const isStartDateBeforeOrEqualMaxDate = maxBookingDate
        ? !dayjsEn(startDate)
              .add(minBookingDays - 1, "day")
              .isAfter(maxBookingDate)
        : true;
    const isEndDatBeforeOrEqualMaxDate = maxBookingDate
        ? !dayjsEn(endDate)
              .add(minBookingDays - 1, "day")
              .isAfter(maxBookingDate)
        : true;

    if (!isStartDateAfterOrEqualMinDate || !isStartDateBeforeOrEqualMaxDate || !isEndDatBeforeOrEqualMaxDate) {
        return false;
    }

    if (minBookingDays === 1 && !endDate && !isDateBlocked(startDate)) {
        return true;
    } else if (
        (minBookingDays > 1 && !endDate && !exactMinBookingDays) ||
        (minBookingDays > 0 && exactMinBookingDays) ||
        (minBookingDays > 0 && exactMinBookingDays && !minBookingDate && !maxBookingDate)
    ) {
        return !getEach(
            startDate,
            dayjsEn(startDate)
                .add(minBookingDays - 1, "day")
                .toDate(),
            "day",
        ).some(d => isDateBlocked(d));
    } else if (endDate && !exactMinBookingDays) {
        const minBookingDaysDate = dayjsEn(startDate)
            .add(minBookingDays - 1, "day")
            .toDate();

        if (dayjsEn(endDate).isBefore(minBookingDaysDate)) {
            return false;
        }

        return !getEach(startDate, endDate, "day").some(d => isDateBlocked(d));
    }

    return false;
}
