import dayjs from "dayjs";
import { getMonths } from "./get-months";

test("getMonths", () => {
    expect(getMonths({ year: 2021 })).toStrictEqual([
        { monthLabel: "January", date: new Date("2021-01-01") },
        { monthLabel: "February", date: new Date("2021-02-01") },
        { monthLabel: "March", date: new Date("2021-03-01") },
        { monthLabel: "April", date: new Date("2021-04-01") },
        { monthLabel: "May", date: new Date("2021-05-01") },
        { monthLabel: "June", date: new Date("2021-06-01") },
        { monthLabel: "July", date: new Date("2021-07-01") },
        { monthLabel: "August", date: new Date("2021-08-01") },
        { monthLabel: "September", date: new Date("2021-09-01") },
        { monthLabel: "October", date: new Date("2021-10-01") },
        { monthLabel: "November", date: new Date("2021-11-01") },
        { monthLabel: "December", date: new Date("2021-12-01") },
    ]);
});

test("getMonths with custom format", () => {
    expect(getMonths({ year: 2021, monthLabelFormat: date => dayjs(date).format("MM") })).toStrictEqual([
        { monthLabel: "01", date: new Date("2021-01-01") },
        { monthLabel: "02", date: new Date("2021-02-01") },
        { monthLabel: "03", date: new Date("2021-03-01") },
        { monthLabel: "04", date: new Date("2021-04-01") },
        { monthLabel: "05", date: new Date("2021-05-01") },
        { monthLabel: "06", date: new Date("2021-06-01") },
        { monthLabel: "07", date: new Date("2021-07-01") },
        { monthLabel: "08", date: new Date("2021-08-01") },
        { monthLabel: "09", date: new Date("2021-09-01") },
        { monthLabel: "10", date: new Date("2021-10-01") },
        { monthLabel: "11", date: new Date("2021-11-01") },
        { monthLabel: "12", date: new Date("2021-12-01") },
    ]);
});
