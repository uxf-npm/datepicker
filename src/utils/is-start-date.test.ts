import { isStartDate } from "./is-start-date";

test("isStartDate", () => {
    expect(isStartDate(new Date("2020-08-08"), new Date("2020-08-08"))).toBe(true);
    expect(isStartDate(new Date("2021-12-15"), new Date("2021-12-15"))).toBe(true);
    expect(isStartDate(new Date("1999-01-05"), new Date("1999-01-05"))).toBe(true);

    expect(isStartDate(new Date("2020-08-09"), new Date("2020-08-08"))).toBe(false);
    expect(isStartDate(new Date("2020-08-10"), new Date("2020-08-08"))).toBe(false);
    expect(isStartDate(new Date("2021-08-08"), new Date("2020-08-08"))).toBe(false);
});
