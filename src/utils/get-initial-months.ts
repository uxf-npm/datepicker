import { getCurrentYearMonthAndDate } from "./get-current-year-month-and-date";
import { getDateMonthAndYear } from "./get-date-month-and-year";
import { MonthType } from "./types";
import { dayjsEn } from "./dayjs-en";

export function getInitialMonths(numberOfMonths: number, startDate: Date | null): MonthType[] {
    const firstMonth = startDate ? getDateMonthAndYear(startDate) : getCurrentYearMonthAndDate();
    let prevMonthDate = firstMonth.date;
    let months = [firstMonth];

    if (numberOfMonths > 1) {
        months = Array.from(Array(numberOfMonths - 1).keys()).reduce((m: MonthType[]) => {
            prevMonthDate = dayjsEn(m[m.length - 1].date)
                .add(1, "month")
                .toDate();
            return m.concat([getDateMonthAndYear(prevMonthDate)]);
        }, months);
    }

    return months;
}
