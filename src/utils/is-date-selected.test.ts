import { isDateInsideRange } from "./is-date-inside-range";

test("isDateInsideRange", () => {
    expect(isDateInsideRange(new Date("2021-08-08"), new Date("2021-06-08"), new Date("2021-08-12"))).toBe(true);
    expect(isDateInsideRange(new Date("2021-08-08"), new Date("2021-08-07"), new Date("2021-08-12"))).toBe(true);
    expect(isDateInsideRange(new Date("2021-08-08"), new Date("2021-06-08"), new Date("2021-08-09"))).toBe(true);

    expect(isDateInsideRange(new Date("2021-08-08"), null, null)).toBe(false);
    expect(isDateInsideRange(new Date("2021-08-08"), new Date("2021-06-08"), null)).toBe(false);
    expect(isDateInsideRange(new Date("2021-08-08"), null, new Date("2021-08-09"))).toBe(false);
    expect(isDateInsideRange(new Date("2021-08-08"), new Date("2021-06-08"), new Date("2021-08-01"))).toBe(false);
    expect(isDateInsideRange(new Date("2021-08-08"), new Date("2021-08-10"), new Date("2021-08-25"))).toBe(false);
});
