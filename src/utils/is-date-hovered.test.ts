import { isDateHovered } from "./is-date-hovered";

test("isDateHovered", () => {
    // exact min booking days
    expect(
        isDateHovered({
            startDate: null,
            endDate: null,
            minBookingDays: 5,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-07"),
            exactMinBookingDays: true,
            isDateBlocked: () => true,
        }),
    ).toBe(false);

    expect(
        isDateHovered({
            startDate: null,
            endDate: null,
            minBookingDays: 5,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-07"),
            exactMinBookingDays: true,
            isDateBlocked: () => false,
        }),
    ).toBe(true);

    // min booking days
    expect(
        isDateHovered({
            startDate: new Date("2021-08-07"),
            endDate: null,
            minBookingDays: 5,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-07"),
            exactMinBookingDays: false,
            isDateBlocked: () => true,
        }),
    ).toBe(false);

    expect(
        isDateHovered({
            startDate: new Date("2021-08-07"),
            endDate: null,
            minBookingDays: 5,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-07"),
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(true);

    // normal
    expect(
        isDateHovered({
            startDate: new Date("2021-08-07"),
            endDate: null,
            minBookingDays: 1,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-09"),
            exactMinBookingDays: false,
            isDateBlocked: () => true,
        }),
    ).toBe(false);

    expect(
        isDateHovered({
            startDate: new Date("2021-08-07"),
            endDate: null,
            minBookingDays: 1,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-09"),
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(true);

    // with endDate
    expect(
        isDateHovered({
            startDate: new Date("2021-08-07"),
            endDate: new Date("2021-08-12"),
            minBookingDays: 1,
            date: new Date("2021-08-08"),
            hoveredDate: new Date("2021-08-07"),
            exactMinBookingDays: false,
            isDateBlocked: () => true,
        }),
    ).toBe(false);
});
