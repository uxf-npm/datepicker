import { isFirstOrLastSelectedDate } from "./is-first-or-last-selected-date";

test("isFirstOrLastSelectedDate", () => {
    expect(isFirstOrLastSelectedDate(new Date("2020-08-08"), new Date("2020-05-08"), new Date("2020-08-08"))).toBe(
        true,
    );
    expect(isFirstOrLastSelectedDate(new Date("2021-12-15"), new Date("2021-12-01"), new Date("2021-12-15"))).toBe(
        true,
    );
    expect(isFirstOrLastSelectedDate(new Date("1999-01-05"), new Date("1999-01-05"), new Date("1999-03-05"))).toBe(
        true,
    );

    expect(isFirstOrLastSelectedDate(new Date("2020-08-09"), new Date("2020-08-08"), new Date("2020-08-10"))).toBe(
        false,
    );
    expect(isFirstOrLastSelectedDate(new Date("2020-08-10"), new Date("2020-08-08"), new Date("2020-12-08"))).toBe(
        false,
    );
    expect(isFirstOrLastSelectedDate(new Date("2021-08-08"), new Date("2020-08-08"), new Date("2020-01-08"))).toBe(
        false,
    );
});
