import { getDateMonthAndYear } from "./get-date-month-and-year";
import { MonthType } from "./types";
import { dayjsEn } from "./dayjs-en";

export function getCurrentYearMonthAndDate(): MonthType {
    return getDateMonthAndYear(dayjsEn().startOf("day").toDate());
}
