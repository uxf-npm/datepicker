import { getNextActiveMonth } from "./get-next-active-month";

test("getNextActiveMonth", () => {
    expect(getNextActiveMonth([{ year: 2021, month: 0, date: new Date("2021-01-01") }], 1, 1)).toStrictEqual([
        { year: 2021, month: 1, date: new Date("2021-02-01") },
    ]);
});

test("getNextActiveMonth with 2 months", () => {
    expect(
        getNextActiveMonth(
            [
                { year: 2021, month: 0, date: new Date("2021-01-01") },
                { year: 2021, month: 1, date: new Date("2021-02-01") },
            ],
            2,
            1,
        ),
    ).toStrictEqual([
        { year: 2021, month: 2, date: new Date("2021-03-01") },
        { year: 2021, month: 3, date: new Date("2021-04-01") },
    ]);
});

test("getNextActiveMonth with 2 months and step", () => {
    expect(
        getNextActiveMonth(
            [
                { year: 2021, month: 0, date: new Date("2021-01-01") },
                { year: 2021, month: 1, date: new Date("2021-02-01") },
            ],
            2,
            1,
            1,
        ),
    ).toStrictEqual([
        { year: 2021, month: 1, date: new Date("2021-02-01") },
        { year: 2021, month: 2, date: new Date("2021-03-01") },
    ]);
});

test("getNextActiveMonth with 2 months and counter 0", () => {
    expect(
        getNextActiveMonth(
            [
                { year: 2021, month: 0, date: new Date("2021-01-01") },
                { year: 2021, month: 1, date: new Date("2021-02-01") },
            ],
            2,
            0,
        ),
    ).toStrictEqual([
        { year: 2021, month: 1, date: new Date("2021-02-01") },
        { year: 2021, month: 0, date: new Date("2021-01-01") },
    ]);
});

test("getNextActiveMonth with 2 months and step and counter 0", () => {
    expect(
        getNextActiveMonth(
            [
                { year: 2021, month: 0, date: new Date("2021-01-01") },
                { year: 2021, month: 1, date: new Date("2021-02-01") },
            ],
            2,
            0,
            1,
        ),
    ).toStrictEqual([
        { year: 2021, month: 2, date: new Date("2021-03-01") },
        { year: 2021, month: 1, date: new Date("2021-02-01") },
    ]);
});

test("getNextActiveMonth with 2 months and negative counter", () => {
    expect(
        getNextActiveMonth(
            [
                { year: 2021, month: 0, date: new Date("2021-01-01") },
                { year: 2021, month: 1, date: new Date("2021-02-01") },
            ],
            2,
            -1,
        ),
    ).toStrictEqual([
        { year: 2020, month: 10, date: new Date("2020-11-01") },
        { year: 2020, month: 11, date: new Date("2020-12-01") },
    ]);
});

test("getNextActiveMonth with 2 months and step and negative counter", () => {
    expect(
        getNextActiveMonth(
            [
                { year: 2021, month: 0, date: new Date("2021-01-01") },
                { year: 2021, month: 1, date: new Date("2021-02-01") },
            ],
            2,
            -1,
            1,
        ),
    ).toStrictEqual([
        { year: 2020, month: 11, date: new Date("2020-12-01") },
        { year: 2021, month: 0, date: new Date("2021-01-01") },
    ]);
});
