import { isInUnavailableDates } from "./is-in-unavailable-dates";

test("isInUnavailableDates", () => {
    expect(
        isInUnavailableDates(
            [new Date("2020-08-08"), new Date("2021-08-08"), new Date("2020-12-08")],
            new Date("2020-08-08"),
        ),
    ).toBe(true);
    expect(isInUnavailableDates([new Date("2021-12-15")], new Date("2021-12-15"))).toBe(true);
    expect(isInUnavailableDates([new Date("2020-08-08"), new Date("1999-01-05")], new Date("1999-01-05"))).toBe(true);

    expect(isInUnavailableDates(undefined, new Date("2020-08-08"))).toBe(false);
    expect(isInUnavailableDates([], new Date("2020-08-08"))).toBe(false);
    expect(isInUnavailableDates([new Date("2021-08-12")], new Date("2020-08-08"))).toBe(false);
});
