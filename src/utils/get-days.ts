import { getEach } from "./get-each";
import { FirstDayOfWeek } from "./types";
import { dayjsEn } from "./dayjs-en";

export interface GetDaysProps {
    year: number;
    month: number;
    firstDayOfWeek?: FirstDayOfWeek;
    dayLabelFormat?: (date: Date) => string;
}

export type CalendarDay = { dayLabel: string; date: Date; currentMonth: boolean };

export function getDays({
    year,
    month,
    firstDayOfWeek = 0,
    dayLabelFormat = (date: Date) => dayjsEn(date).format("DD"),
}: GetDaysProps): CalendarDay[] {
    const date = new Date(year, month);

    const lastDayOfWeek = firstDayOfWeek + 7;
    const monthStart = dayjsEn(date).startOf("month").toDate();
    const monthStartDay = dayjsEn(monthStart).day();
    const monthEnd = dayjsEn(date).endOf("month").toDate();
    const monthEndDay = dayjsEn(monthEnd).day();

    const prevMonthDaysCount =
        monthStartDay >= firstDayOfWeek ? monthStartDay - firstDayOfWeek : 6 - firstDayOfWeek + monthStartDay + 1;

    const prevMonthDays =
        prevMonthDaysCount > 0
            ? getEach(
                  dayjsEn(monthStart).subtract(prevMonthDaysCount, "days").toDate(),
                  dayjsEn(monthStart).subtract(1, "day").toDate(),
                  "day",
              ).map(d => ({
                  currentMonth: false,
                  date: d,
                  dayLabel: dayLabelFormat(d),
              }))
            : [];

    const nextMonthDaysCount = lastDayOfWeek - monthEndDay - 1;

    const nextMonthDays =
        nextMonthDaysCount > 0 && nextMonthDaysCount < 7
            ? getEach(
                  dayjsEn(monthEnd).add(1, "day").toDate(),
                  dayjsEn(monthEnd).add(nextMonthDaysCount, "days").toDate(),
                  "day",
              ).map(d => ({
                  currentMonth: false,
                  date: d,
                  dayLabel: dayLabelFormat(d),
              }))
            : [];

    const days = getEach(monthStart, monthEnd, "day").map(d => ({
        currentMonth: true,
        date: d,
        dayLabel: dayLabelFormat(d),
    }));

    return [...prevMonthDays, ...days, ...nextMonthDays];
}
