import dayjs from "dayjs";
import { getWeekdayLabels } from "./get-weekday-labels";
import "dayjs/locale/cs";

test("getWeekdayLabels", () => {
    expect(getWeekdayLabels({})).toStrictEqual(["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]);
});

test("getWeekdayLabels with custom firstDayOfWeek", () => {
    expect(getWeekdayLabels({ firstDayOfWeek: 1 })).toStrictEqual(["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]);
});

test("getWeekdayLabels with custom format", () => {
    expect(getWeekdayLabels({ weekdayLabelFormat: date => dayjs(date).format("dddd") })).toStrictEqual([
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ]);
});

test("getWeekdayLabels with custom format and locale", () => {
    expect(
        getWeekdayLabels({ firstDayOfWeek: 1, weekdayLabelFormat: date => dayjs(date).locale("cs").format("dddd") }),
    ).toStrictEqual(["pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota", "neděle"]);
});
