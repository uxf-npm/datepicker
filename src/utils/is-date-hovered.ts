import { getEach } from "./get-each";
import { dayjsEn } from "./dayjs-en";

export interface IsDateHoveredProps {
    startDate: Date | null;
    endDate: Date | null;
    date: Date;
    isDateBlocked(date: Date): boolean;
    hoveredDate: Date | null;
    minBookingDays: number;
    exactMinBookingDays: boolean;
}

export function isDateHovered({
    date,
    startDate,
    endDate,
    isDateBlocked,
    hoveredDate,
    minBookingDays,
    exactMinBookingDays,
}: IsDateHoveredProps) {
    if (
        // exact min booking days
        hoveredDate &&
        minBookingDays > 1 &&
        exactMinBookingDays &&
        dayjsEn(date).isBetween(hoveredDate, dayjsEn(hoveredDate).add(minBookingDays - 1, "day"))
    ) {
        return !getEach(
            hoveredDate,
            dayjsEn(hoveredDate)
                .add(minBookingDays - 1, "day")
                .toDate(),
            "day",
        ).some(d => isDateBlocked(d));
    } else if (
        // min booking days
        startDate &&
        !endDate &&
        hoveredDate &&
        dayjsEn(date).isBetween(startDate, dayjsEn(startDate).add(minBookingDays - 1, "day")) &&
        dayjsEn(startDate).isSame(hoveredDate, "days") &&
        minBookingDays > 1
    ) {
        return !getEach(
            startDate,
            dayjsEn(startDate)
                .add(minBookingDays - 1, "day")
                .toDate(),
            "day",
        ).some(d => isDateBlocked(d));
    } else if (
        // normal
        startDate &&
        !endDate &&
        hoveredDate &&
        !dayjsEn(hoveredDate).isBefore(startDate) &&
        dayjsEn(date).isBetween(startDate, hoveredDate)
    ) {
        return !getEach(startDate, hoveredDate, "day").some(d => isDateBlocked(d));
    }
    return false;
}
