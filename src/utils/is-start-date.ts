import { dayjsEn } from "./dayjs-en";

export function isStartDate(date: Date, startDate: Date | null) {
    return !!(startDate && dayjsEn(date).isSame(startDate, "day"));
}
