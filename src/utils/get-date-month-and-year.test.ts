import { getDateMonthAndYear } from "./get-date-month-and-year";

test("getDateMonthAndYear", () => {
    expect(getDateMonthAndYear(new Date("2021-08-10"))).toStrictEqual({
        year: 2021,
        month: 7,
        date: new Date("2021-08-01"),
    });

    expect(getDateMonthAndYear(new Date("2020-02-15"))).toStrictEqual({
        year: 2020,
        month: 1,
        date: new Date("2020-02-01"),
    });
});
