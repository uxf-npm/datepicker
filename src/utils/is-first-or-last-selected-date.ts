import { dayjsEn } from "./dayjs-en";

export function isFirstOrLastSelectedDate(date: Date, startDate: Date | null, endDate: Date | null) {
    return !!(
        (startDate && dayjsEn(date).isSame(startDate, "day")) ||
        (endDate && dayjsEn(date).isSame(endDate, "day"))
    );
}
