import dayjs from "dayjs";
import { getYears } from "./get-years";

test("getYears", () => {
    expect(getYears({ startYear: new Date("2018-08-15"), endYear: new Date("2021-05-03") })).toStrictEqual([
        { yearLabel: "2018", date: new Date("2018-01-01") },
        { yearLabel: "2019", date: new Date("2019-01-01") },
        { yearLabel: "2020", date: new Date("2020-01-01") },
        { yearLabel: "2021", date: new Date("2021-01-01") },
    ]);
});

test("getYears with custom format", () => {
    expect(
        getYears({
            startYear: new Date("2018-08-15"),
            endYear: new Date("2021-05-03"),
            yearLabelFormat: date => dayjs(date).format("YY"),
        }),
    ).toStrictEqual([
        { yearLabel: "18", date: new Date("2018-01-01") },
        { yearLabel: "19", date: new Date("2019-01-01") },
        { yearLabel: "20", date: new Date("2020-01-01") },
        { yearLabel: "21", date: new Date("2021-01-01") },
    ]);
});
