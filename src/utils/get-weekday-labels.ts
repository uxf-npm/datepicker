import { getEach } from "./get-each";
import { FirstDayOfWeek } from "./types";
import { dayjsEn } from "./dayjs-en";

export interface GetWeekdayLabelsProps {
    firstDayOfWeek?: FirstDayOfWeek;
    weekdayLabelFormat?: (date: Date) => string;
}

export function getWeekdayLabels({
    firstDayOfWeek = 0,
    weekdayLabelFormat = (date: Date) => dayjsEn(date).format("dd"),
}: GetWeekdayLabelsProps): string[] {
    const now = new Date();
    const arr = getEach(
        dayjsEn(now).startOf("week").add(firstDayOfWeek, "day").toDate(),
        dayjsEn(now).endOf("week").add(firstDayOfWeek, "day").toDate(),
        "day",
    );
    return arr.map(date => weekdayLabelFormat(date));
}
