import { dayjsEn } from "./dayjs-en";

export function isDateInsideRange(date: Date, startDate: Date | null, endDate: Date | null) {
    if (startDate && endDate) {
        return dayjsEn(date).isBetween(startDate, endDate);
    }
    return false;
}
