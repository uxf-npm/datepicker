import { getEach } from "./get-each";
import { dayjsEn } from "./dayjs-en";

export interface GetMonthsProps {
    year: number;
    monthLabelFormat?(date: Date): string;
}

export type CalendarMonth = { monthLabel: string; date: Date };

export function getMonths({
    year,
    monthLabelFormat = (date: Date) => dayjsEn(date).format("MMMM"),
}: GetMonthsProps): CalendarMonth[] {
    const date = new Date(year, 0);

    const yearStart = dayjsEn(date).startOf("year").toDate();
    const yearEnd = dayjsEn(date).endOf("year").toDate();

    return getEach(yearStart, yearEnd, "month").map(d => ({
        date: d,
        monthLabel: monthLabelFormat(d),
    }));
}
