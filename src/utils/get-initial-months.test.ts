import { getInitialMonths } from "./get-initial-months";

test("getInitialMonths", () => {
    expect(getInitialMonths(1, new Date("2021-09-15"))).toStrictEqual([
        {
            date: new Date("2021-09-01"),
            month: 8,
            year: 2021,
        },
    ]);
});

test("getInitialMonths 2 months", () => {
    expect(getInitialMonths(2, new Date("2021-09-15"))).toStrictEqual([
        {
            date: new Date("2021-09-01"),
            month: 8,
            year: 2021,
        },
        {
            date: new Date("2021-10-01"),
            month: 9,
            year: 2021,
        },
    ]);
});

test("getInitialMonths without date", () => {
    jest.useFakeTimers("modern");
    jest.setSystemTime(new Date("2021-09-15"));

    expect(getInitialMonths(1, null)).toStrictEqual([
        {
            date: new Date("2021-09-01"),
            month: 8,
            year: 2021,
        },
    ]);
});
