import { dayjsEn } from "./dayjs-en";

export function isEndDate(date: Date, endDate: Date | null) {
    return !!(endDate && dayjsEn(date).isSame(endDate, "day"));
}
