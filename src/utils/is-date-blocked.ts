import { isInUnavailableDates } from "./is-in-unavailable-dates";
import { dayjsEn } from "./dayjs-en";

interface IsDateBlockedProps {
    date: Date;
    startDate: Date | null;
    endDate: Date | null;
    minBookingDays?: number;
    minBookingDate?: Date;
    maxBookingDate?: Date;
    isDateBlockedFn?: (date: Date) => boolean;
    unavailableDates?: Date[];
}

export function isDateBlocked({
    date,
    minBookingDate,
    maxBookingDate,
    isDateBlockedFn,
    startDate,
    endDate,
    minBookingDays = 1,
    unavailableDates = [],
}: IsDateBlockedProps) {
    const compareMinDate = minBookingDate
        ? new Date(minBookingDate.getFullYear(), minBookingDate.getMonth(), minBookingDate.getDate(), 0, 0, 0)
        : minBookingDate;
    const compareMaxDate = maxBookingDate
        ? new Date(maxBookingDate.getFullYear(), maxBookingDate.getMonth(), maxBookingDate.getDate(), 0, 0, 0)
        : maxBookingDate;
    return !!(
        isInUnavailableDates(unavailableDates, date) ||
        (compareMinDate && dayjsEn(date).isBefore(compareMinDate)) ||
        (compareMaxDate && dayjsEn(date).isAfter(compareMaxDate)) ||
        (startDate &&
            !endDate &&
            minBookingDays > 1 &&
            dayjsEn(date).isBetween(startDate, dayjsEn(startDate).add(minBookingDays - 2, "day"))) ||
        (isDateBlockedFn && isDateBlockedFn(date))
    );
}
