import { canSelectRange } from "./can-select-range";

test("canSelectRange", () => {
    // start day only
    expect(
        canSelectRange({
            startDate: new Date("2021-08-08"),
            endDate: null,
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(true);

    // start and end date
    expect(
        canSelectRange({
            startDate: new Date("2021-08-08"),
            endDate: new Date("2021-08-08"),
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(true);

    // minBookingDays longer than range
    expect(
        canSelectRange({
            startDate: new Date("2021-08-08"),
            endDate: new Date("2021-08-08"),
            minBookingDays: 2,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(false);

    // minBookingDays longer than range
    expect(
        canSelectRange({
            startDate: new Date("2021-08-08"),
            endDate: new Date("2021-08-08"),
            minBookingDays: 2,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(false);

    // blocked date
    expect(
        canSelectRange({
            startDate: new Date("2021-08-08"),
            endDate: new Date("2021-08-08"),
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            exactMinBookingDays: false,
            isDateBlocked: () => true,
        }),
    ).toBe(false);

    // before minBookingDate
    expect(
        canSelectRange({
            startDate: new Date("2021-08-05"),
            endDate: new Date("2021-08-12"),
            minBookingDays: 1,
            minBookingDate: new Date("2021-08-10"),
            maxBookingDate: undefined,
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(false);

    // after maxBookingDate
    expect(
        canSelectRange({
            startDate: new Date("2021-08-01"),
            endDate: new Date("2021-08-08"),
            minBookingDays: 1,
            minBookingDate: undefined,
            maxBookingDate: new Date("2021-08-06"),
            exactMinBookingDays: false,
            isDateBlocked: () => false,
        }),
    ).toBe(false);

    // exactMinBookingDays
    expect(
        canSelectRange({
            startDate: new Date("2021-08-01"),
            endDate: new Date("2021-08-08"),
            minBookingDays: 8,
            minBookingDate: undefined,
            maxBookingDate: undefined,
            exactMinBookingDays: true,
            isDateBlocked: () => false,
        }),
    ).toBe(true);
});
