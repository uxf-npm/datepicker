module.exports = {
    extends: ["@uxf/eslint-config"],
    ignorePatterns: ["dist/", "demo/", "coverage/"],
    rules: {
        "import/no-restricted-paths": [
            "error",
            {
                zones: [
                    {
                        target: "./src/**/!(dayjs-en.ts|*.test.ts)",
                        from: "./node_modules/dayjs",
                    },
                ],
            },
        ],
    },
};
