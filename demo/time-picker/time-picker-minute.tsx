import React, { FC, useContext, useRef } from "react";
import { useMinute } from "../../src/hooks/use-minute";
import { TimePickerContext } from "../../src/contexts/time-picker-context";

export const TimePickerMinute: FC<{ minute: number; label: string }> = ({ minute, label }) => {
    const minuteRef = useRef(null);
    const { focusedMinute, isMinuteFocused, isMinuteSelected, onMinuteSelect, onMinuteFocus } =
        useContext(TimePickerContext);

    const { isSelected, onClick, onKeyDown, tabIndex } = useMinute({
        minute,
        minuteRef,
        focusedMinute,
        isMinuteFocused,
        isMinuteSelected,
        onMinuteFocus,
        onMinuteSelect,
    });

    const background = isSelected ? "blue" : "yellow";

    return (
        <button
            type="button"
            tabIndex={tabIndex}
            onKeyDown={onKeyDown}
            onClick={onClick}
            className="day"
            ref={minuteRef}
            style={{
                background: background,
                padding: 15,
                display: "inline-block",
            }}
        >
            {label}
        </button>
    );
};
