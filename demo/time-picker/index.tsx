import React, { FC, useCallback, useState } from "react";
import { TimePickerContext } from "../../src/contexts/time-picker-context";
import { TimeType, useTimePicker } from "../../src/hooks/use-time-picker";
import { TimePickerHours } from "./time-picker-hours";
import { TimePickerMinutes } from "./time-picker-minutes";

export const TimePicker: FC = () => {
    const [selectedTime, setSelectedTime] = useState<TimeType | null>(null);
    const [viewMode, setViewMode] = useState<"global" | "hour" | "minute">("global");

    const onTimeChange = useCallback<(data: TimeType) => void>(data => {
        setSelectedTime(data);
    }, []);

    const { goToNextMinute, goToPrevHour, goToPrevMinute, onTimeSelect, goToNextHour, ...contextProps } = useTimePicker(
        {
            selectedTime,
            onTimeChange,
            onSelectCallback: () => setViewMode("global"),
        },
    );

    return (
        <TimePickerContext.Provider value={{ ...contextProps }}>
            <div>
                {
                    {
                        global: (
                            <div style={{ display: "flex" }}>
                                <div>
                                    <button onClick={goToPrevHour}>prev</button>
                                    <button onClick={() => setViewMode("hour")} style={{ fontSize: 32 }}>
                                        {selectedTime?.hour ?? "00"}
                                    </button>
                                    <button onClick={goToNextHour}>next</button>
                                </div>
                                <div>&nbsp;:&nbsp;</div>
                                <div>
                                    <button onClick={goToPrevMinute}>prev</button>
                                    <button onClick={() => setViewMode("minute")} style={{ fontSize: 32 }}>
                                        {selectedTime?.minute ?? "00"}
                                    </button>
                                    <button onClick={goToNextMinute}>next</button>
                                </div>
                            </div>
                        ),
                        hour: <TimePickerHours />,
                        minute: <TimePickerMinutes />,
                    }[viewMode]
                }
            </div>
            <div>
                {selectedTime?.hour}:{selectedTime?.minute}
            </div>
        </TimePickerContext.Provider>
    );
};
