import React, { FC } from "react";
import { TimePickerHour } from "./time-picker-hour";
import { useHours } from "../../src/hooks/use-hours";

export const TimePickerHours: FC = () => {
    const { hours } = useHours({});

    return (
        <div>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(4, 80px)" }}>
                {hours.map(hour => (
                    <TimePickerHour hour={hour.value} label={hour.label} key={hour.value} />
                ))}
            </div>
        </div>
    );
};
