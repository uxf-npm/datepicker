import React, { FC } from "react";
import { useMinutes } from "../../src/hooks/use-minutes";
import { TimePickerMinute } from "./time-picker-minute";

export const TimePickerMinutes: FC = () => {
    const { minutes } = useMinutes({});

    return (
        <div>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(4, 80px)" }}>
                {minutes.map(minute => (
                    <TimePickerMinute minute={minute.value} label={minute.label} key={minute.value} />
                ))}
            </div>
        </div>
    );
};
