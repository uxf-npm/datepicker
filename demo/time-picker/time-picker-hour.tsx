import React, { FC, useContext, useRef } from "react";
import { useHour } from "../../src/hooks/use-hour";
import { TimePickerContext } from "../../src/contexts/time-picker-context";

export const TimePickerHour: FC<{ hour: number; label: string }> = ({ hour, label }) => {
    const hourRef = useRef(null);
    const { focusedHour, isHourFocused, isHourSelected, onHourSelect, onHourFocus } = useContext(TimePickerContext);

    const { onClick, onKeyDown, tabIndex, isSelected } = useHour({
        hour,
        hourRef,
        isHourFocused,
        focusedHour,
        isHourSelected,
        onHourSelect,
        onHourFocus,
    });

    const background = isSelected ? "blue" : "yellow";

    return (
        <button
            type="button"
            tabIndex={tabIndex}
            onKeyDown={onKeyDown}
            onClick={onClick}
            className="day"
            ref={hourRef}
            style={{
                background: background,
                padding: 15,
                display: "inline-block",
            }}
        >
            {label}
        </button>
    );
};
