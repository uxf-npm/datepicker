import React, { FC, useContext } from "react";
import { DatePickerContext } from "../../src/contexts/date-picker-context";
import { useMonth, UseMonthProps } from "../../src/hooks/use-month";
import { DatePickerDay } from "./date-picker-day";

export const DatePickerMonth: FC<UseMonthProps> = ({ month, year }) => {
    const { canGoToNextMonth, canGoToPrevMonth, goToNextMonthsByOneMonth, goToPrevMonthsByOneMonth, firstDayOfWeek } =
        useContext(DatePickerContext);

    const { days, monthLabel } = useMonth({
        year,
        month,
        firstDayOfWeek,
    });

    return (
        <div>
            <div>
                <a
                    onClick={canGoToPrevMonth ? goToPrevMonthsByOneMonth : undefined}
                    style={{ color: !canGoToPrevMonth ? "lightGray" : undefined }}
                >
                    Previous month
                </a>
                <a
                    onClick={canGoToNextMonth ? goToNextMonthsByOneMonth : undefined}
                    style={{ color: !canGoToNextMonth ? "lightGray" : undefined }}
                >
                    Next month
                </a>
            </div>
            <p>{monthLabel}</p>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(7, 80px)" }}>
                {days.map((day, index) => (
                    <DatePickerDay
                        currentMonth={day.currentMonth}
                        date={day.date}
                        day={day.dayLabel}
                        key={day.dayLabel + index}
                    />
                ))}
            </div>
        </div>
    );
};
