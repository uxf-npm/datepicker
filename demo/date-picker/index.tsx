import React, { FC, useCallback, useState } from "react";
import { DatePickerContext } from "../../src/contexts/date-picker-context";
import { OnDateChangeType, useDatePicker } from "../../src/hooks/use-date-picker";
import { DatePickerDecade } from "./date-picker-decade";
import { DatePickerMonth } from "./date-picker-month";
import { DatePickerYear } from "./date-picker-year";
import { dayjsEn } from "../../src/utils/dayjs-en";

const MIN_BOOKING_DATE = new Date("2020-01-31");
const MAX_BOOKING_DATE = dayjsEn().add(1, "month").toDate();

export const DatePicker: FC = () => {
    const [selectedDate, setSelectedDate] = useState<Date | null>(null);
    const [viewMode, setViewMode] = useState<"month" | "year" | "decade">("month");

    const onDateChange = useCallback<(data: OnDateChangeType) => void>(data => {
        setSelectedDate(data);
    }, []);

    const {
        activeMonths,
        focusedDate,
        goToDate,
        isDateBlocked,
        isDateFocused,
        isDateHovered,
        isDateSelected,
        onDateFocus,
        onDateHover,
        onDateSelect,
        ...restProps
    } = useDatePicker({
        selectedDate,
        minBookingDate: MIN_BOOKING_DATE,
        maxBookingDate: MAX_BOOKING_DATE,
        onDateChange: onDateChange,
    });

    return (
        <DatePickerContext.Provider
            value={{
                activeMonths,
                focusedDate,
                goToDate,
                isDateBlocked,
                isDateFocused,
                isDateHovered,
                isDateSelected,
                onDateFocus,
                onDateHover,
                onDateSelect,
                ...restProps,
            }}
        >
            <div>
                <>
                    {
                        {
                            month: (
                                <>
                                    <a onClick={() => setViewMode("year")}>změnit měsíc</a>
                                    {activeMonths.map(month => (
                                        <DatePickerMonth
                                            key={`${month.year}-${month.month}`}
                                            month={month.month}
                                            year={month.year}
                                        />
                                    ))}
                                </>
                            ),
                            year: (
                                <>
                                    <a onClick={() => setViewMode("decade")}>změnit rok</a>
                                    <DatePickerYear
                                        year={activeMonths[0].year}
                                        onMonthSelect={date => {
                                            goToDate(date);
                                            setViewMode("month");
                                        }}
                                    />
                                </>
                            ),
                            decade: (
                                <DatePickerDecade
                                    year={activeMonths[0].year}
                                    onYearSelect={date => {
                                        goToDate(date);
                                        setViewMode("year");
                                    }}
                                />
                            ),
                        }[viewMode]
                    }
                </>
            </div>
            <div>{selectedDate?.toDateString()}</div>
        </DatePickerContext.Provider>
    );
};
