import React, { FC, useContext, useRef } from "react";
import { DatePickerContext } from "../../src/contexts/date-picker-context";
import { useDay } from "../../src/hooks/use-day";

export const DatePickerDay: FC<{ day: string; date: Date; currentMonth: boolean }> = ({ currentMonth, date, day }) => {
    const dayRef = useRef(null);
    const {
        focusedDate,
        isDateFocused,
        isDateSelected,
        isDateHovered,
        isDateBlocked,
        onDateSelect,
        onDateFocus,
        onDateHover,
    } = useContext(DatePickerContext);

    const { disabledDate, isSelected, isWithinHoverRange, onClick, onKeyDown, onMouseEnter, tabIndex, isToday } =
        useDay({
            date,
            dayRef,
            focusedDate,
            isDateBlocked,
            isDateFocused,
            isDateHovered,
            isDateSelected,
            onDateFocus,
            onDateHover,
            onDateSelect,
        });

    if (!day) {
        return <div />;
    }

    const background =
        disabledDate || !currentMonth
            ? "lightGray"
            : isWithinHoverRange
            ? "lightYellow"
            : isSelected
            ? "blue"
            : isToday
            ? "gray"
            : "yellow";

    return (
        <button
            type="button"
            tabIndex={tabIndex}
            onKeyDown={onKeyDown}
            onClick={onClick}
            onMouseEnter={onMouseEnter}
            className="day"
            ref={currentMonth ? dayRef : undefined}
            style={{
                background: background,
                padding: 15,
                display: "inline-block",
            }}
        >
            {day}
        </button>
    );
};
