import dayjs from "dayjs";
import React, { FC, useContext } from "react";
import { DatePickerContext } from "../../src/contexts/date-picker-context";
import { useDecade, UseDecadeProps } from "../../src/hooks/use-decade";

interface DatePickerYearProps extends UseDecadeProps {
    onYearSelect: (date: Date) => void;
}

export const DatePickerDecade: FC<DatePickerYearProps> = ({ onYearSelect, year: thisYear }) => {
    const { decadeLabel, years } = useDecade({
        year: thisYear,
    });

    const { canGoToYear, goToNextYear, goToPrevYear } = useContext(DatePickerContext);

    return (
        <div>
            <p>
                <a
                    onClick={
                        canGoToYear(dayjs(new Date(thisYear, 0)).subtract(4, "years").toDate())
                            ? () => goToPrevYear(9)
                            : undefined
                    }
                >
                    Předchozí
                </a>{" "}
                {decadeLabel}{" "}
                <a
                    onClick={
                        canGoToYear(dayjs(new Date(thisYear, 0)).add(4, "years").toDate())
                            ? () => goToNextYear(9)
                            : undefined
                    }
                >
                    Následující
                </a>
            </p>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(3, 188px)" }}>
                {years.map((year, index) => (
                    <span
                        key={year.yearLabel + index}
                        onClick={() => (canGoToYear(year.date) ? onYearSelect(year.date) : undefined)}
                        style={{
                            padding: 15,
                            display: "inline-block",
                            background: canGoToYear(year.date) ? "yellow" : "lightgray",
                        }}
                    >
                        {year.yearLabel}
                    </span>
                ))}
            </div>
        </div>
    );
};
