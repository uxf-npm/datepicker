import React, { FC, useContext } from "react";
import { DatePickerContext } from "../../src/contexts/date-picker-context";
import { useYear, UseYearProps } from "../../src/hooks/use-year";

interface DatePickerYearProps extends UseYearProps {
    onMonthSelect: (date: Date) => void;
}

export const DatePickerYear: FC<DatePickerYearProps> = ({ onMonthSelect, year }) => {
    const { yearLabel, months } = useYear({
        year,
    });

    const { canGoToMonth, canGoToNextYear, canGoToPrevYear, goToNextYear, goToPrevYear } =
        useContext(DatePickerContext);

    return (
        <div>
            <p>
                <a onClick={canGoToPrevYear ? () => goToPrevYear(1) : undefined}>Předchozí rok</a> {yearLabel}{" "}
                <a onClick={canGoToNextYear ? () => goToNextYear(1) : undefined}>Následující rok</a>
            </p>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(3, 188px)" }}>
                {months.map((month, index) => (
                    <span
                        key={month.monthLabel + index}
                        onClick={() => (canGoToMonth(month.date) ? onMonthSelect(month.date) : undefined)}
                        style={{
                            padding: 15,
                            display: "inline-block",
                            background: canGoToMonth(month.date) ? "yellow" : "lightgray",
                        }}
                    >
                        {month.monthLabel}
                    </span>
                ))}
            </div>
        </div>
    );
};
