import React, { FC, useContext } from "react";
import { DateRangePickerContext } from "../../src/contexts/date-range-picker-context";
import { useMonth, UseMonthProps } from "../../src/hooks/use-month";
import { DateRangePickerDay } from "./date-range-picker-day";

export const DateRangePickerMonth: FC<UseMonthProps> = ({ month, year }) => {
    const { canGoToNextMonth, canGoToPrevMonth, goToNextMonthsByOneMonth, goToPrevMonthsByOneMonth, firstDayOfWeek } =
        useContext(DateRangePickerContext);

    const { days, monthLabel } = useMonth({
        year,
        month,
        firstDayOfWeek,
    });

    return (
        <div style={{ padding: 5 }}>
            <div>
                <a
                    onClick={canGoToPrevMonth ? goToPrevMonthsByOneMonth : undefined}
                    style={{ color: !canGoToPrevMonth ? "lightGray" : undefined }}
                >
                    Previous month
                </a>
                <a
                    onClick={canGoToNextMonth ? goToNextMonthsByOneMonth : undefined}
                    style={{ color: !canGoToNextMonth ? "lightGray" : undefined }}
                >
                    Next month
                </a>
            </div>
            <p>{monthLabel}</p>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(7, 80px)" }}>
                {days.map((day, index) => (
                    <DateRangePickerDay
                        currentMonth={day.currentMonth}
                        date={day.date}
                        day={day.dayLabel}
                        key={day.dayLabel + index}
                    />
                ))}
            </div>
        </div>
    );
};
