import dayjs from "dayjs";
import React, { FC, useCallback, useState } from "react";
import { DateRangePickerContext } from "../../src/contexts/date-range-picker-context";
import { OnDatesChangeProps, START_DATE, useDateRangePicker } from "../../src/hooks/use-date-range-picker";
import { DateRangePickerMonth } from "./date-range-picker-month";

const MIN_BOOKING_DATE = new Date();
const MAX_BOOKING_DATE = new Date("2023-06-12");

export const DateRangePicker: FC = () => {
    const [fromTo, setFromTo] = useState<{ from: Date | null; to: Date | null }>({ from: null, to: null });

    const [state, setState] = useState<OnDatesChangeProps>(() => ({
        startDate: fromTo.from ? dayjs(fromTo.from, "D/M/YYYY").toDate() : null,
        endDate: fromTo.to ? dayjs(fromTo.to, "D/M/YYYY").toDate() : null,
        focusedInput: START_DATE,
    }));

    const onDatesChange = useCallback<(data: OnDatesChangeProps) => void>(data => {
        setFromTo({ from: data.startDate, to: data.endDate });
        setState({ ...data, focusedInput: data.focusedInput ?? START_DATE });
    }, []);

    const { activeMonths, ...restProps } = useDateRangePicker({
        ...state,
        changeActiveMonthOnSelect: false,
        minBookingDate: MIN_BOOKING_DATE,
        maxBookingDate: MAX_BOOKING_DATE,
        numberOfMonths: 2,
        onDatesChange,
    });

    return (
        <DateRangePickerContext.Provider
            value={{
                activeMonths,
                ...restProps,
            }}
        >
            <div style={{ display: "flex", flexWrap: "wrap" }}>
                {activeMonths.map(month => (
                    <DateRangePickerMonth key={`${month.year}-${month.month}`} month={month.month} year={month.year} />
                ))}
            </div>
            <div>{fromTo.from?.toDateString()}</div>
            <div>{fromTo.to?.toDateString()}</div>
        </DateRangePickerContext.Provider>
    );
};
