import React, { FC, useContext, useRef } from "react";
import { DateRangePickerContext } from "../../src/contexts/date-range-picker-context";
import { useDay } from "../../src/hooks/use-day";

export const DateRangePickerDay: FC<{ day: string; date: Date; currentMonth: boolean }> = ({
    currentMonth,
    date,
    day,
}) => {
    const dayRef = useRef(null);
    const {
        focusedDate,
        isDateFocused,
        isDateSelected,
        isDateHovered,
        isDateInsideRange,
        isDateBlocked,
        onDateSelect,
        onDateFocus,
        onDateHover,
    } = useContext(DateRangePickerContext);

    const {
        disabledDate,
        isSelected,
        isWithinHoverRange,
        isInsideRange,
        onClick,
        onKeyDown,
        onMouseEnter,
        tabIndex,
        isToday,
    } = useDay({
        date,
        focusedDate,
        isDateBlocked,
        isDateFocused,
        isDateHovered,
        isDateInsideRange,
        isDateSelected,
        onDateFocus,
        onDateHover,
        onDateSelect,
        dayRef,
    });

    if (!day) {
        return <div />;
    }

    const background =
        disabledDate || !currentMonth
            ? "lightGray"
            : isWithinHoverRange
            ? "lightYellow"
            : isInsideRange
            ? "lightBlue"
            : isSelected
            ? "blue"
            : isToday
            ? "gray"
            : "yellow";

    return (
        <button
            type="button"
            tabIndex={tabIndex}
            onKeyDown={onKeyDown}
            onClick={onClick}
            onMouseEnter={onMouseEnter}
            className="day"
            ref={currentMonth ? dayRef : undefined}
            style={{
                background: background,
                padding: 15,
                display: "inline-block",
            }}
        >
            {day}
        </button>
    );
};
