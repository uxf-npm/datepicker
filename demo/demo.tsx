import React from "react";
import ReactDOM from "react-dom";
import { DatePicker } from "./date-picker";
import { DateRangePicker } from "./date-range-picker";
import { TimePicker } from "./time-picker";

export default class App extends React.Component {
    render() {
        return (
            <>
                <DateRangePicker />
                <hr />
                <DatePicker />
                <hr />
                <TimePicker />
            </>
        );
    }
}

const domContainer = document.querySelector("#app");
ReactDOM.render(<App />, domContainer);
